# TeamIce
Web-aplikacija za koordinaciju sudionika akcija spašavanja

## URL web stranice
https://hgss-tracks.herokuapp.com/

## Opis
HGSStracks je aplikacija koja olakšava koordinaciju rada svih ljudi aktivnih na akciji spašavanja.  

Prilikom registracije moguće je odabrati ulogu dispečera ili spasioca te je potrebno navesti korisničke podatke.
Nakon slanja zahtjeva za registraciju, za prijavu u sustav potrebno je pričekati potvrdu administratora.

Svaki spasilac pripada nekoj stanici. Spasilac redovito osvježava podatak o dostupnosti za akcije. Tragovi kuda su spasioci prošli se bilježe kako bi se mogli dispečeru prikazati u obliku toplinske karte ukoliko to zatraži.

Voditelj je glavni spasilac u stanici i on se isto može odazvati na akciju spašavanja. Voditelj spasiocima u svojoj stanici definira na koji način su osposobljeni voditi spašavanje (auto, bicikl, pješke, pas, dron, helikopter, brod). 

Dispečer na temelju vanjske prijave otvara akcije spašavanja s informacijama o nestaloj osobi. Ako je akcija spašavanja završila, dispečer je u sustavu može označiti kao gotovom. Dispečer vidi broj dostupnih spasioca po stanicama i može poslati zahtjev za uključivanjem dodatnih spasilaca u akciju spašavanja. Prilikom slanja zahtjeva dispečer definira na koji način bi spasilac trebao sudjelovati (auto, pješke.. ). Spasioci koji zadovoljavaju kriterije se mogu odazvati na akciju. Dispečer, ako je to potrebno, može spasioca ukloniti s akcije.

Dispečer spasiocima pojedinačno zadaje zadatke. Zadaci mogu biti: prođi određenom rutom, dođi do lokacije, postavi privremenu postaju, osvježi zalihe u privremenoj postaji. Svaki zadatak može imati i dodatan komentar od dispečera. Za izračun ruta koje prate staze i ceste koristi se vanjski servis OSRM. Spasiocu se na karti prikazuju zadaci koje treba obaviti, privremene i stalne postaje i trenutna pozicija ostalih spasioca aktivnih na istoj akciji, a na karti može ostaviti i komentar za ostale sudionike u akciji.


## Autori

* **Antea Ribarić**
* **Domagoj Blažanin**
* **Dominik Rozić**
* **Tin Dinarina Mladić**
* **Marin Ujević**
* **Matko Pribičević**
* **Zvonimir Krišto**