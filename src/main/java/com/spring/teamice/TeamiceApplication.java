package com.spring.teamice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TeamiceApplication {
    public static void main(String[] args){
        SpringApplication.run(TeamiceApplication.class, args);
    }
}
