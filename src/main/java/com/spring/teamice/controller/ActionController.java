package com.spring.teamice.controller;

import com.spring.teamice.model.*;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

@Controller
public class ActionController {

    @ModelAttribute("headerName")
    public String headerName(@SessionAttribute User loggedInUser) {
        if (loggedInUser != null)
            return "Ulogiran kao: " + loggedInUser.getFirstName() + " " + loggedInUser.getSurname();
        else return null;
    }

    @ModelAttribute("loggedInUser")
    public User loggedInUser(@SessionAttribute User loggedInUser) {
        return loggedInUser;
    }

    @ModelAttribute("loggedInSaviorInAction")
    public boolean loggedInSaviorInAction(@SessionAttribute User loggedInUser) {
        if (!loggedInUser.isSavior()) return false;
        System.out.println("loggedInSaviorInAction: " + !DatabaseUtil.getSaviorById(loggedInUser.getUserId()).getAvailable());
        return !DatabaseUtil.getSaviorById(loggedInUser.getUserId()).getAvailable();
    }

    @ModelAttribute("loggedInSaviorsQualifications")
    public ArrayList<QualificationName> loggedInSaviorsQualifications(@SessionAttribute User loggedInUser) {
        if (!loggedInUser.isSavior()) return new ArrayList<>();
        return DatabaseUtil.getSaviorById(loggedInUser.getUserId()).getQualificationsEnum();
    }

    @ModelAttribute("actions")
    public List<Action> actions(@SessionAttribute User loggedInUser) {
        if (loggedInUser.isDispatcher() || loggedInUser.isAdmin()) return DatabaseUtil.getAllActions();
        else if (loggedInUser.isSavior()) {
            System.out.println(Savior.getAllJoinableActions(loggedInUser.getUserId()));
            return Savior.getAllJoinableActions(loggedInUser.getUserId());
        } else return new ArrayList<>();
    }

    @ModelAttribute("stations")
    public List<Station> stations(@SessionAttribute User loggedInUser) {
        return DatabaseUtil.getAllStations();
    }

    @ModelAttribute("qualificationTypes")
    public List<QualificationName> qualificationTypes() {
        return new ArrayList<>(EnumSet.allOf(QualificationName.class));
    }

    @GetMapping("/actions")
    public String actionSummary() {
        return "actions";
    }

    @PostMapping("/joinAction")
    public String joinAction(@SessionAttribute User loggedInUser, @RequestParam String actionId, @RequestParam QualificationName qualification) {
        if (!loggedInUser.isSavior()) return "redirect:/invalidCredentials";
        DatabaseUtil.getSaviorById(loggedInUser.getUserId()).joinAction(actionId, qualification);
        return "redirect:/map";
    }

    @PostMapping("/requestSaviors")
    public String requestSaviors(@SessionAttribute User loggedInUser, @RequestParam String actionId, @RequestParam String stationId, @RequestParam String qualification, @RequestParam int quantity) {
        if (!loggedInUser.isDispatcher()) return "redirect:/invalidCredentials";
        Dispatcher.sendActionRequest(new ActionRequest(actionId, stationId, qualification, quantity));
        return "actions";
    }

    @PostMapping("/closeAction")
    public String requestSaviors(@SessionAttribute User loggedInUser, @RequestParam String actionId) {
        if (!loggedInUser.isDispatcher()) return "redirect:/invalidCredentials";
        Dispatcher.closeAction(actionId);
        return "actions";
    }

    @PostMapping("/removeFromAction")
    public String removeFromAction(@SessionAttribute User loggedInUser, @RequestParam String saviorId) {
        Dispatcher.removeSaviorFromAction(saviorId);
        return "actions";
    }
}