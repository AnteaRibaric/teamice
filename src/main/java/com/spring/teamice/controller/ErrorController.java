package com.spring.teamice.controller;

import org.springframework.web.bind.annotation.GetMapping;

public class ErrorController {
    @GetMapping("/error")
    public String home() {
        return "error";
    }
}
