package com.spring.teamice.controller;

import com.spring.teamice.model.DatabaseUtil;
import com.spring.teamice.model.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttribute;

@Controller
public class HomeController {

    @ModelAttribute("headerName")
    public String headerName(@SessionAttribute(required = false) User loggedInUser) {
        if (loggedInUser != null)
            return "Ulogiran kao: " + loggedInUser.getFirstName() + " " + loggedInUser.getSurname();
        else return null;
    }

    @ModelAttribute("loggedInUser")
    public User loggedInUser(@SessionAttribute(required = false) User loggedInUser) {
        return loggedInUser;
    }

    @ModelAttribute("loggedInSaviorInAction")
    public Boolean loggedInSaviorInAction(@SessionAttribute(required = false) User loggedInUser) {
        if (loggedInUser == null) return null;
        if (!loggedInUser.isSavior()) return false;
        System.out.println("loggedInSaviorInAction: " + !DatabaseUtil.getSaviorById(loggedInUser.getUserId()).getAvailable());
        return !DatabaseUtil.getSaviorById(loggedInUser.getUserId()).getAvailable();
    }

    @GetMapping(value = {"", "/", "/home"})
    public String home() {
        return "home";
    }

    @RequestMapping("/createDatabase")
    public String createDatabase() {
        DatabaseUtil.createNewDatabase();
        return "redirect:/home";
    }
}
