package com.spring.teamice.controller;

import com.spring.teamice.model.DatabaseUtil;
import com.spring.teamice.model.LoginStatus;
import com.spring.teamice.model.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;


@Controller
public class LoginController {
    @ModelAttribute("headerName")
    public String headerName(@SessionAttribute(required = false) User loggedInUser) {
        if (loggedInUser != null)
            return "Ulogiran kao: " + loggedInUser.getFirstName() + " " + loggedInUser.getSurname();
        else return null;
    }

    @ModelAttribute("loggedInUser")
    public User loggedInSavior(@SessionAttribute(required = false) User loggedInUser) {
        return loggedInUser;
    }

    @ModelAttribute("loggedInSaviorInAction")
    public Boolean loggedInSaviorInAction(@SessionAttribute(required = false) User loggedInUser) {
        if (loggedInUser == null) return null;
        if (!loggedInUser.isSavior()) return false;
        System.out.println("loggedInSaviorInAction: " + !DatabaseUtil.getSaviorById(loggedInUser.getUserId()).getAvailable());
        return !DatabaseUtil.getSaviorById(loggedInUser.getUserId()).getAvailable();
    }

    @GetMapping("/login")
    public String home(Model model, HttpSession session) {
        if (session.getAttribute("loggedInUser") == null) {
            model.addAttribute("loggedIn", false);
        }
        return "/login";
    }

    @PostMapping("/logInUser")
    public String logInUser(String username, String password, HttpServletRequest request, RedirectAttributes redirAttrs) {
        LoginStatus loginStatus = User.successfulLogin(username, password);
        if (loginStatus.getSuccessfullLogin()) {
            String userId = loginStatus.getUserId();
            User loggedInUser = User.getUserById(userId);
            if (userId != null && loggedInUser != null) {
                request.getSession().setAttribute("loggedInUser", loggedInUser);
                System.out.println("User logged in: " + loggedInUser.getUsername());
                return "redirect:/home";
            }
        } else redirAttrs.addFlashAttribute("error", loginStatus.getMessage());
        return "redirect:/login";
    }
}
