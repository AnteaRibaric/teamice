package com.spring.teamice.controller;

import com.spring.teamice.model.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.SessionAttribute;

import javax.servlet.http.HttpSession;


@Controller
public class LogoutController {

    @GetMapping("/logout")
    public String userScreen(HttpSession session, @SessionAttribute User loggedInUser) {
        System.out.println("User logged out: " + loggedInUser.getUsername());
        session.invalidate();
        return "redirect:/home";
    }
}
