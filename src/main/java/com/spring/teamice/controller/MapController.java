package com.spring.teamice.controller;

import com.spring.teamice.model.*;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.*;

@Controller
public class MapController {
    public ArrayList<String> permissions = new ArrayList<>(Arrays.asList(
            UserType.ADMIN.toString(), UserType.DISPATCHER.toString(),
            UserType.LEAD_SAVIOR.toString(), UserType.SAVIOR.toString()));

    @ModelAttribute("headerName")
    public String headerName(HttpSession session) {
        if (session.getAttribute("loggedInUser") != null) {
            User loggedInUser = (User) session.getAttribute("loggedInUser");
            return "Ulogiran kao: " + loggedInUser.getFirstName() + " " + loggedInUser.getSurname();
        } else return null;
    }

    @ModelAttribute("loggedInUser")
    public User loggedInUser(@SessionAttribute(required = false) User loggedInUser) {
        return loggedInUser;
    }

    @ModelAttribute("validCredentials")
    public Boolean validCredentials(HttpSession session, @SessionAttribute User loggedInUser) {
        String loggedInRole = loggedInUser.getUserType().toString();
        session.setAttribute("validCredentials", permissions.contains(loggedInRole));
        if (permissions.contains(loggedInRole)) {
            return Boolean.TRUE;
        } else {
            System.out.println("Invalid credentials:" + loggedInRole + "is not in " + permissions);
            return Boolean.FALSE;
        }
    }

    @ModelAttribute("loggedInSaviorInAction")
    public boolean loggedInSaviorInAction(@SessionAttribute User loggedInUser) {
        if (!loggedInUser.isSavior()) return false;
        System.out.println("loggedInSaviorInAction: " + !DatabaseUtil.getSaviorById(loggedInUser.getUserId()).getAvailable());
        return !DatabaseUtil.getSaviorById(loggedInUser.getUserId()).getAvailable();
    }

    @ModelAttribute("actionSaviors")
    public List<Savior> actionSaviors(@SessionAttribute User loggedInUser) {
        if (loggedInUser.isSavior()) {
            ActionInfo actionInfo = DatabaseUtil.getSaviorById(loggedInUser.getUserId()).currentActionInfo();
            if (actionInfo != null)
                return DatabaseUtil.getActionById(actionInfo.getActionId()).fetchSaviorsInAction();
        } else if (loggedInUser.isDispatcher()) {
            return DatabaseUtil.getAllSaviors();
        }
        return new ArrayList<>();
    }

    @ModelAttribute("stations")
    public List<Station> stations(@SessionAttribute User loggedInUser) {
        return DatabaseUtil.getAllStations();
    }

    @ModelAttribute("comments")
    public List<MapComment> comments(@SessionAttribute User loggedInUser) {
        if (loggedInUser.isSavior()) {
            return DatabaseUtil.getCurrentActionComments(loggedInUser.getUserId());
        } else if (loggedInUser.isDispatcher()) return DatabaseUtil.getAllMapComments();
        else return new ArrayList<>();
    }

    @ModelAttribute("actions")
    public List<Action> actions(@SessionAttribute User loggedInUser) {
        ArrayList<Action> actions = new ArrayList<>();
        if (loggedInUser.isDispatcher()) return DatabaseUtil.getAllActions();
        else if (loggedInUser.isSavior()) {
            String actionId = DatabaseUtil.getSaviorById(loggedInUser.getUserId()).getCurrentActionId();
            if (actionId != null) {
                actions.add(DatabaseUtil.getActionById(actionId));
            }
        }
        return actions;
    }

    @ModelAttribute("wayPoints")
    public List<WayPoint> wayPoints(@SessionAttribute User loggedInUser) {
        if (loggedInUser.isSavior()) return DatabaseUtil.getAllUserWayPoints(loggedInUser.getUserId());
        return new ArrayList<>();
    }

    @ModelAttribute("tasks")
    public List<Task> tasks(@SessionAttribute User loggedInUser) {
        if (loggedInUser.isSavior())
            return DatabaseUtil.getAllUsersTasks(loggedInUser.getUserId());
        else if (loggedInUser.isDispatcher())
            return DatabaseUtil.getAllTasks();
        else return new ArrayList<>();
    }

    @ModelAttribute("taskTypes")
    public List<TaskType> taskTypes() {
        return new ArrayList<>(EnumSet.allOf(TaskType.class));
    }

    @ModelAttribute("stationTypes")
    public List<StationType> stationTypes() {
        return new ArrayList<>(EnumSet.allOf(StationType.class));
    }

    @RequestMapping("/action")
    public String action(@RequestParam("text") String text, @RequestParam("location") String location, @SessionAttribute User loggedInUser) {
        if (!loggedInUser.isDispatcher()) return "redirect:/invalidCredentials";
        new Action(text, new Location(location), true).storeAction();
        return "redirect:/map";
    }

    @RequestMapping("/comment")
    public String comment(@RequestParam("text") String text, @RequestParam("location") String location, @RequestParam("actionId") String actionId, @SessionAttribute User loggedInUser) {
        if (!(loggedInUser.isDispatcher() || (loggedInUser.isSavior() && DatabaseUtil.getActionById(actionId).fetchSaviorIdsInAction().contains(loggedInUser.getUserId()))))
            return "redirect:/invalidCredentials";
        new MapComment(actionId, text, new Location(location)).storeMapComment();
        return "redirect:/map";
    }

    @RequestMapping("/waypoint")
    public String waypoint(@RequestParam("location") String location, @SessionAttribute User loggedInUser) {
        if (!loggedInUser.isSavior()) return "redirect:/invalidCredentials";
        new WayPoint(loggedInUser.getUserId(), String.valueOf(UUID.randomUUID()), new Location(location)).storeWayPoint();
        return "redirect:/map";
    }

    @RequestMapping("/createStation")
    public String createStation(@RequestParam("text") String text, @RequestParam("location") String location, @RequestParam("stationType") StationType stationType, @SessionAttribute User loggedInUser) {
        if (!loggedInUser.isAdmin()) return "redirect:/invalidCredentials";
        new Station(text, stationType, new Location(location)).storeStation();
        return "redirect:/map";
    }

    @RequestMapping("/assignTask")
    public String assignTask(@RequestParam("text") String text, @RequestParam("location") String location, @RequestParam("taskType") TaskType taskType, @RequestParam("actionSavior") String saviorId, @RequestParam("actionId") String actionId, @SessionAttribute User loggedInUser) {
        if (!loggedInUser.isDispatcher()) return "redirect:/invalidCredentials";
        Dispatcher.assignSaviorTask(saviorId, actionId, location, taskType, text);
        return "redirect:/map";
    }

    @RequestMapping("/deleteMarker")
    public String deleteMarker(@RequestParam("markerId") String id, @SessionAttribute Boolean validCredentials) {
        if (!validCredentials) return "redirect:/invalidCredentials";
        for (WayPoint m : DatabaseUtil.getAllWayPoints()) {
            if (id.equals(m.getWayPointId())) {
                m.deleteWaypoint();
                return "redirect:/map";
            }
        }
        for (MapComment m : DatabaseUtil.getAllMapComments()) {
            if (id.equals(m.getCommentId())) {
                m.deleteComment();
                return "redirect:/map";
            }
        }
        for (Task m : DatabaseUtil.getAllTasks()) {
            if (id.equals(m.getTaskId())) {
                m.deleteTask();
                return "redirect:/map";
            }
        }
        for (Action m : DatabaseUtil.getAllActions()) {
            if (id.equals(m.getActionId())) {
                Dispatcher.closeAction(m.getActionId());
                return "redirect:/map";
            }
        }
        for (Station m : DatabaseUtil.getAllStations()) {
            if (id.equals(m.getStationId())) {
                m.deleteStation();
                return "redirect:/map";
            }
        }
        return "redirect:/error";

    }

    @GetMapping("/map")
    public String map(@SessionAttribute Boolean validCredentials) {
        if (!validCredentials) return "redirect:/invalidCredentials";
        return "/map";
    }

    @RequestMapping(value = "/coordinates", method = RequestMethod.POST)
    public @ResponseBody
    Map<Float, List<List<Float>>> saveCoordinates(@RequestBody String data, @SessionAttribute User loggedInUser) {
        if (loggedInUser.isDispatcher()) return HeatTrace.generateHeatData(HeatTrace.getAllHeatTraces());
        if (!loggedInUser.isSavior()) return new HashMap<>();
        Savior loggedInSavior = DatabaseUtil.getSaviorById(loggedInUser.getUserId());
        assert loggedInSavior != null;
        QualificationName saviorQualification = loggedInSavior.getCurrentActionQualification();
        if (saviorQualification == null) {
            return new HashMap<>();
        }
        try {
            JSONObject js = new JSONObject(data);
            Location loc = new Location(Float.parseFloat((String) js.get("latitude")), Float.parseFloat((String) js.get("longitude")));
            int radius = saviorQualification.coverage();
            loggedInSavior.updateLocation(loc);
            HeatTrace.storeHeatTrace(new HeatTrace(loggedInSavior.getUserId(), loc, 100 - radius, radius));
        } catch (JSONException e) {
            System.out.println("Greška prilikom parsiranja JSON-a" + data);
        }
        return HeatTrace.generateHeatData(HeatTrace.getAllHeatTraces());
    }
}