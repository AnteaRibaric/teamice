package com.spring.teamice.controller;

import com.spring.teamice.model.*;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@Controller
public class ProfileController {

    public ArrayList<String> permissions = new ArrayList<>(Arrays.asList(
            UserType.ADMIN.toString(), UserType.DISPATCHER.toString(),
            UserType.LEAD_SAVIOR.toString(), UserType.SAVIOR.toString()));

    @ModelAttribute("loggedInUser")
    public User loggedInUser(@SessionAttribute User loggedInUser) {
        return loggedInUser;
    }

    @ModelAttribute("available")
    public boolean available(@SessionAttribute User loggedInUser) {
        if (loggedInUser.isSavior())
            return DatabaseUtil.getSaviorById(loggedInUser.getUserId()).getAvailable();
        else return false;
    }

    @ModelAttribute("validCredentials")
    public Boolean validCredentials(@SessionAttribute User loggedInUser) {
        String loggedInRole = loggedInUser.getUserType().toString();
        if (permissions.contains(loggedInRole)) return Boolean.TRUE;
        else {
            System.out.println("Invalid credentials:" + loggedInRole + "is not in " + permissions);
            return Boolean.FALSE;
        }
    }

    @ModelAttribute("headerName")
    public String headerName(@SessionAttribute User loggedInUser) {
        return "Ulogiran kao: " + loggedInUser.getFirstName() + " " + loggedInUser.getSurname();
    }

    @ModelAttribute("loggedInSaviorInAction")
    public boolean loggedInSaviorInAction(@SessionAttribute User loggedInUser) {
        if (!loggedInUser.isSavior()) return false;
        System.out.println("loggedInSaviorInAction: " + !DatabaseUtil.getSaviorById(loggedInUser.getUserId()).getAvailable());
        return !DatabaseUtil.getSaviorById(loggedInUser.getUserId()).getAvailable();
    }

    @GetMapping("/profile")
    public String home() {
        return "/profile";
    }

    @RequestMapping(value = "/available", method = RequestMethod.POST)
    public @ResponseBody Boolean setAvailable(@RequestBody String data, @SessionAttribute User loggedInUser) {
        DatabaseUtil.getSaviorById(loggedInUser.getUserId()).updateAvailability(Boolean.parseBoolean(data));
        return Boolean.parseBoolean(data);
    }
}
