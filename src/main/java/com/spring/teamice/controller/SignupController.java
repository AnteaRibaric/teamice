package com.spring.teamice.controller;

import com.spring.teamice.model.DatabaseUtil;
import com.spring.teamice.model.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Base64;

@Controller
public class SignupController {
    @ModelAttribute("loggedInUser")
    public User loggedInUser(HttpSession session) {
        return (User) session.getAttribute("loggedInUser");
    }

    @ModelAttribute("loggedInSaviorInAction")
    public Boolean loggedInSaviorInAction(@SessionAttribute(required = false) User loggedInUser) {
        if (loggedInUser == null) return null;
        if (!loggedInUser.isSavior()) return false;
        System.out.println("loggedInSaviorInAction: " + !DatabaseUtil.getSaviorById(loggedInUser.getUserId()).getAvailable());
        return !DatabaseUtil.getSaviorById(loggedInUser.getUserId()).getAvailable();
    }

    @GetMapping("/signup")
    public String home(Model model) {
        model.addAttribute("user", new User());
        return "signup";
    }

    @PostMapping("/signUpUser")
    public String signUpUser(@ModelAttribute User user, RedirectAttributes redirAttrs, @RequestParam(name = "photoUser") MultipartFile photoUser) throws IOException {
        String encodedString = Base64.getEncoder().encodeToString(photoUser.getBytes());
        byte[] bytesPhoto = encodedString.getBytes();
        user.setPhoto(bytesPhoto);
        if (user.signUpIncomplete()) {
            redirAttrs.addFlashAttribute("error", "Neuspjela registracija, nepotpuni podaci za prijavu");
            return "redirect:/signup";
        } else if (User.usernameTaken(user.getUsername())) {
            System.out.println("Username: " + user.getUsername() + " is taken");
            redirAttrs.addFlashAttribute("error", "Neuspjela registracija, korisničko ime je zauzeto");
            return "redirect:/signup";

        } else {
            user.registerUser();
            return "redirect:/home";
        }
    }
}
