package com.spring.teamice.controller;

import com.spring.teamice.model.*;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Controller
public class StationsController {
    @ModelAttribute("headerName")
    public String headerName(@SessionAttribute User loggedInUser) {
        if (loggedInUser != null)
            return "Ulogiran kao: " + loggedInUser.getFirstName() + " " + loggedInUser.getSurname();
        else return null;
    }

    @ModelAttribute("loggedInUser")
    public User loggedInUser(@SessionAttribute User loggedInUser) {
        return loggedInUser;
    }

    @ModelAttribute("loggedInSaviorInAction")
    public boolean loggedInSaviorInAction(@SessionAttribute User loggedInUser) {
        if (loggedInUser == null) return false;
        if (!loggedInUser.isSavior()) return false;
        System.out.println("loggedInSaviorInAction: " + !DatabaseUtil.getSaviorById(loggedInUser.getUserId()).getAvailable());
        return !DatabaseUtil.getSaviorById(loggedInUser.getUserId()).getAvailable();
    }

    @ModelAttribute("stations")
    public List<Station> stations(@SessionAttribute User loggedInUser) {
        if (loggedInUser.isAdmin() || loggedInUser.isDispatcher()) return DatabaseUtil.getAllStations();
        else if (loggedInUser.isLeadSavior())
            return new ArrayList<>(Collections.singletonList(DatabaseUtil.getStationById(DatabaseUtil.getSaviorById(loggedInUser.getUserId()).getStationId())));
        else return new ArrayList<>();
    }

    @ModelAttribute("unassignedSaviors")
    public List<Savior> unassignedSaviors(@SessionAttribute User loggedInUser) {
        if (loggedInUser.isDispatcher() || (loggedInUser.isSavior() && !loggedInUser.isLeadSavior()))
            return new ArrayList<>();
        return Savior.getUnassignedSaviors();
    }

    @GetMapping("/stations")
    public String stationScreen(@SessionAttribute User loggedInUser) {
        if (loggedInUser.isSavior() && !loggedInUser.isLeadSavior()) return "redirect:/home";
        return "/stations";
    }

    @RequestMapping("/editStation")
    public String editStation(@RequestParam(value = "Edit") String stationId, Model model, @SessionAttribute User loggedInUser) {
        if (loggedInUser.isDispatcher() || (loggedInUser.isSavior() && !loggedInUser.isLeadSavior()))
            return "redirect:/home";
        model.addAttribute("editStationId", stationId);
        model.addAttribute("newStation", new Station());
        return "stations";
    }

    @RequestMapping("/stopStationEdit")
    public String stopEditStation(Model model, @SessionAttribute User loggedInUser) {
        if (loggedInUser.isDispatcher() || (loggedInUser.isSavior() && !loggedInUser.isLeadSavior()))
            return "redirect:/home";
        model.addAttribute("editStationId", null);
        return "redirect:/stations";
    }

    @PostMapping("/updateStation")
    public String updateStation(@ModelAttribute Station station, @SessionAttribute User loggedInUser) {
        if (loggedInUser.isDispatcher() || (loggedInUser.isSavior() && !loggedInUser.isLeadSavior()))
            return "redirect:/home";
        station.updateStation();
        return "redirect:/stations";
    }

    @RequestMapping("/deleteStation")
    public String deleteStation(@RequestParam String stationId, @SessionAttribute User loggedInUser) {
        if (loggedInUser.isDispatcher() || (loggedInUser.isSavior() && !loggedInUser.isLeadSavior()))
            return "redirect:/home";
        Station.deleteStation(stationId);
        return "redirect:/stations";
    }

    @RequestMapping("/assignSaviorToStation")
    public String assignSaviorToStation(@RequestParam String stationId, @RequestParam String userId, @SessionAttribute User loggedInUser) {
        if (loggedInUser.isDispatcher() || (loggedInUser.isSavior() && !loggedInUser.isLeadSavior()))
            return "redirect:/home";
        Savior.assignToStation(stationId, userId);
        return "redirect:/stations";
    }

    @RequestMapping("/removeSaviorFromStation")
    public String removeSaviorFromStation(@RequestParam String saviorId, @SessionAttribute User loggedInUser) {
        if (loggedInUser.isDispatcher() || (loggedInUser.isSavior() && !loggedInUser.isLeadSavior()))
            return "redirect:/home";
        Savior.removeFromStation(saviorId);
        return "redirect:/stations";
    }

    @RequestMapping("/makeLeadSavior")
    public String makeLeadSavior(@RequestParam String userId, @SessionAttribute User loggedInUser) {
        if (!loggedInUser.isAdmin()) return "redirect:/home";
        Savior.makeLead(userId);
        return "redirect:/stations";
    }

    @RequestMapping("/makeSavior")
    public String makeSavior(@RequestParam String saviorId, @SessionAttribute User loggedInUser) {
        if (!loggedInUser.isAdmin()) return "redirect:/home";
        Savior.makeSavior(saviorId);
        return "redirect:/stations";
    }

    @RequestMapping("/addQualification")
    public String addQualification(@RequestParam String saviorId, @RequestParam String qualification, @SessionAttribute User loggedInUser) {
        if (!loggedInUser.isLeadSavior()) return "redirect:/home";
        LeadSavior.addSaviorQualification(saviorId, QualificationName.toEnum(qualification));
        return "redirect:/stations";
    }
}
