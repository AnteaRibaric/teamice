package com.spring.teamice.controller;

import com.spring.teamice.model.DatabaseUtil;
import com.spring.teamice.model.User;
import com.spring.teamice.model.UserType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


@Controller
public class UserController {

    public ArrayList<String> permissions = new ArrayList<>(Arrays.asList(UserType.ADMIN.toString()));

    @ModelAttribute("users")
    public List<User> users() {
        return DatabaseUtil.getAllUsers();
    }

    @ModelAttribute("headerName")
    public String headerName(@SessionAttribute User loggedInUser) {
        if (loggedInUser != null)
            return "Ulogiran kao: " + loggedInUser.getFirstName() + " " + loggedInUser.getSurname();
        else return null;
    }

    @ModelAttribute("loggedInUser")
    public User loggedInUser(@SessionAttribute User loggedInUser) {
        return loggedInUser;
    }

    @ModelAttribute("loggedInSaviorInAction")
    public boolean loggedInSaviorInAction(@SessionAttribute User loggedInUser) {
        if (!loggedInUser.isSavior()) return false;
        System.out.println("loggedInSaviorInAction: " + !DatabaseUtil.getSaviorById(loggedInUser.getUserId()).getAvailable());
        return !DatabaseUtil.getSaviorById(loggedInUser.getUserId()).getAvailable();
    }

    @ModelAttribute("validCredentials")
    public Boolean validCredentials(HttpSession session, @SessionAttribute User loggedInUser) {
        String loggedInRole = loggedInUser.getUserType().toString();
        session.setAttribute("validCredentials", permissions.contains(loggedInRole));
        if (permissions.contains(loggedInRole)) {
            return Boolean.TRUE;
        } else {
            System.out.println("Invalid credentials:" + loggedInRole + "is not in " + permissions);
            return Boolean.FALSE;
        }
    }

    @GetMapping("/users")
    public String userScreen(@SessionAttribute User loggedInUser, Model model, @SessionAttribute Boolean validCredentials) {
        if (!validCredentials) return "redirect:/invalidCredentials";
        model.addAttribute("userName", loggedInUser.getUsername());
        return "users";
    }

    @RequestMapping("/acceptUser")
    public String acceptUser(@RequestParam(value = "Accept") String userId, @SessionAttribute Boolean validCredentials) {
        if (!validCredentials) return "redirect:/invalidCredentials";
        System.out.println("Accepted user: " + userId);
        User.acceptUser(userId);
        return "redirect:/users";
    }

    @RequestMapping("/deleteUser")
    public String declineUser(@RequestParam(value = "Delete") String userId, @SessionAttribute Boolean validCredentials) {
        if (!validCredentials) return "redirect:/invalidCredentials";
        User.deleteUser(userId);
        return "redirect:/users";
    }

    @RequestMapping("/editUser")
    public String editUser(@RequestParam(value = "Edit") String userId, Model model, @SessionAttribute Boolean validCredentials) {
        if (!validCredentials) return "redirect:/invalidCredentials";
        model.addAttribute("editUserId", userId);
        model.addAttribute("newUser", new User());
        return "users";
    }

    @RequestMapping("/stopEdit")
    public String stopEdit(Model model, @SessionAttribute Boolean validCredentials) {
        if (!validCredentials) return "redirect:/invalidCredentials";
        model.addAttribute("editUserId", null);
        return "users";
    }

    @PostMapping("/updateUser")
    public String updateUser(@ModelAttribute User user, @SessionAttribute Boolean validCredentials) {
        if (!validCredentials) return "redirect:/invalidCredentials";
        System.out.println("Update user postmapping: " + user);
        user.updateUser();
        return "redirect:/users";
    }

    @RequestMapping("/deleteAll")
    public String deleteAll(@SessionAttribute Boolean validCredentials) {
        if (!validCredentials) return "redirect:/invalidCredentials";
        DatabaseUtil.deleteAllUsers();
        return "redirect:/users";
    }

    @RequestMapping("/generate")
    public String generate(@SessionAttribute Boolean validCredentials) {
        if (!validCredentials) return "redirect:/invalidCredentials";
        DatabaseUtil.createBasicData();
        return "redirect:/users";
    }
}