package com.spring.teamice.model;

import java.sql.*;
import java.util.ArrayList;
import java.util.UUID;

public class Action {
    private String actionId;
    private String actionInfo;
    private Location location;
    private Boolean active;
    private ArrayList<String> saviorsInAction;

    public Action(String actionId, String actionInfo, Location location, Boolean active) {
        this.actionId = actionId;
        this.actionInfo = actionInfo;
        this.location = location;
        this.active = active;
    }

    public Action(String actionInfo, Location location, Boolean active) {
        this.actionId = UUID.randomUUID().toString();
        this.actionInfo = actionInfo;
        this.location = location;
        this.active = active;
    }

    public static ArrayList<Action> actionMapper(ResultSet rs) throws SQLException {
        ArrayList<Action> listOfActions = new ArrayList<>();
        while (rs.next()) {
            Action action = new Action(rs.getString("actionId"), rs.getString("description"), new Location(rs.getString("location")), rs.getBoolean("active"));
            listOfActions.add(action);
        }
        return listOfActions;
    }

    public static void removeAllActionSaviors(String actionId) {
        String SQL = "DELETE FROM \"SaviorsInAction\" WHERE \"actionId\" = ?";

        try (Connection conn = DatabaseUtil.connectToDatabase();
             PreparedStatement pstmt = conn.prepareStatement(SQL)) {
            pstmt.setString(1, actionId);
            pstmt.executeUpdate();
            DatabaseUtil.print("All saviors from action removed successfully");
        } catch (SQLException ex) {
            DatabaseUtil.print("Error while removing all saviors from action:" + ex.getMessage());
        }
    }


    public void storeAction() {
        String SQLQuery = "" +
                "INSERT INTO \"Action\"" +
                "VALUES (?,?,?,?)";
        try (Connection conn = DatabaseUtil.connectToDatabase();
             PreparedStatement pstmt = conn.prepareStatement(SQLQuery,
                     Statement.RETURN_GENERATED_KEYS)) {
            pstmt.setString(1, this.getActionId());
            pstmt.setString(2, this.getActionInfo());
            pstmt.setString(3, this.getLocation().toString());
            pstmt.setBoolean(4, this.getActive());

            int affectedRows = pstmt.executeUpdate();
            if (affectedRows > 0) {
                DatabaseUtil.print("Action stored successfully: " + this);
            }
        } catch (SQLException ex) {
            DatabaseUtil.err("Error while storing action: " + ex.getMessage());
        }
    }

    public String getActionId() {
        return actionId;
    }

    public void setActionId(String actionId) {
        this.actionId = actionId;
    }

    public String getActionInfo() {
        return actionInfo;
    }

    public void setActionInfo(String actionInfo) {
        this.actionInfo = actionInfo;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public ArrayList<String> fetchSaviorIdsInAction() {
        String SQL = "SELECT \"userId\" FROM \"SaviorsInAction\" WHERE \"actionId\" = '" + actionId + "';";
        ArrayList<String> listOfSaviors = new ArrayList<>();
        try (Connection conn = DatabaseUtil.connectToDatabase();
             Statement stmt = conn.createStatement();
             ResultSet rs = stmt.executeQuery(SQL)) {
            while (rs.next()) {
                listOfSaviors.add(rs.getString("userId"));
            }
            DatabaseUtil.print("Successfully retrieved savior ids in action: " + listOfSaviors);
        } catch (SQLException ex) {
            DatabaseUtil.err("Error when getting all savior ids: " + ex.getMessage());
            return new ArrayList<>();
        }
        saviorsInAction = listOfSaviors;
        return saviorsInAction;
    }

    public ArrayList<Savior> fetchSaviorsInAction() {
        String SQL = "SELECT \"userId\" FROM \"SaviorsInAction\" WHERE \"actionId\" = '" + actionId + "';";
        ArrayList<Savior> listOfSaviors = new ArrayList<>();
        ArrayList<String> listOfSaviorIds = new ArrayList<>();
        try (Connection conn = DatabaseUtil.connectToDatabase();
             Statement stmt = conn.createStatement();
             ResultSet rs = stmt.executeQuery(SQL)) {
            while (rs.next()) {
                listOfSaviorIds.add(rs.getString("userId"));
            }
            DatabaseUtil.print("Successfully retrieved saviors in action: " + listOfSaviors);
        } catch (SQLException ex) {
            DatabaseUtil.err("Error when getting all saviors in action: " + ex.getMessage());
            return new ArrayList<>();
        }
        for (Savior s : DatabaseUtil.getAllSaviors()) {
            if (listOfSaviorIds.contains(s.getUserId())) {
                listOfSaviors.add(s);
            }
        }
        return listOfSaviors;
    }

    public void setSaviorsInAction(ArrayList<String> saviorsInAction) {
        this.saviorsInAction = saviorsInAction;
    }

    @Override
    public String toString() {
        return "Action{" +
                "actionId='" + actionId + '\'' +
                ", actionInfo='" + actionInfo + '\'' +
                ", location=" + location +
                ", active=" + active +
                ", saviorsInAction=" + saviorsInAction +
                '}';
    }
}
