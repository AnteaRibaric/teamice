package com.spring.teamice.model;

public class ActionInfo {
    private String actionId;
    private String userId;
    private QualificationName saviorQualification;

    public ActionInfo(String actionId, String userId, String qualificationId) {
        this.actionId = actionId;
        this.userId = userId;
        this.saviorQualification = QualificationName.toEnum(qualificationId);
    }

    public String getActionId() {
        return actionId;
    }

    public void setActionId(String actionId) {
        this.actionId = actionId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public QualificationName getSaviorQualification() {
        return saviorQualification;
    }

    public void setSaviorQualification(QualificationName saviorQualification) {
        this.saviorQualification = saviorQualification;
    }

    @Override
    public String toString() {
        return "ActionInfo{" +
                "actionId='" + actionId + '\'' +
                ", userId='" + userId + '\'' +
                ", saviorQualification=" + saviorQualification +
                '}';
    }
}
