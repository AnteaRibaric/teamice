package com.spring.teamice.model;

import java.sql.*;
import java.util.ArrayList;

public class ActionRequest {
    private String actionId;
    private String stationId;
    private QualificationName qualification;
    private int numberOfQualified;

    public ActionRequest(String actionId, String stationId, String qualification, int numberOfQualified) {
        this.actionId = actionId;
        this.stationId = stationId;
        this.qualification = QualificationName.toEnum(qualification);
        this.numberOfQualified = numberOfQualified;
    }

    public static ArrayList<ActionRequest> requestMapper(ResultSet rs) throws SQLException {
        ArrayList<ActionRequest> actionRequests = new ArrayList<>();
        while (rs.next()) {
            ActionRequest savior = new ActionRequest(rs.getString("actionId"), rs.getString("stationId"), rs.getString("qualificationId"), rs.getInt("numberOfQualified"));
            actionRequests.add(savior);
        }
        return actionRequests;
    }

    public static void deleteActionRequests(String actionId) {
        String SQL = "DELETE FROM \"ActionRequests\" WHERE \"actionId\" = ?";

        try (Connection conn = DatabaseUtil.connectToDatabase();
             PreparedStatement pstmt = conn.prepareStatement(SQL)) {
            pstmt.setString(1, actionId);
            pstmt.executeUpdate();
        } catch (SQLException ex) {
            DatabaseUtil.print("Error while deleting action requests:" + ex.getMessage());
        }
        DatabaseUtil.print("Action requests deleted successfully");
    }

    public static void updateRequestNumber(String actionId, String stationId, QualificationName qualificationName) {
        String decrementSQL = "UPDATE \"ActionRequests\" SET \"numberOfQualified\" = \"numberOfQualified\" - 1 " +
                "WHERE \"actionId\" = ? AND \"stationId\" = ? AND \"qualificationId\" = ?;";
        String deleteSQL = "DELETE FROM \"ActionRequests\" WHERE \"numberOfQualified\" = 0;";
        try (Connection conn = DatabaseUtil.connectToDatabase();
             PreparedStatement updateNumber = conn.prepareStatement(decrementSQL);
             PreparedStatement delete = conn.prepareStatement(deleteSQL)) {
            conn.setAutoCommit(false);
            updateNumber.setString(1, actionId);
            updateNumber.setString(2, stationId);
            updateNumber.setString(3, qualificationName.toString());
            updateNumber.executeUpdate();
            delete.executeUpdate();
            conn.commit();
        } catch (SQLException e) {
            DatabaseUtil.err("Error while updating number of requested qualification: " + e.getMessage());
        }
    }

    public void storeRequest() {
        String SQLQuery = "" +
                "INSERT INTO \"ActionRequests\"" +
                "VALUES (?,?,?,?)";
        try (Connection conn = DatabaseUtil.connectToDatabase();
             PreparedStatement pstmt = conn.prepareStatement(SQLQuery,
                     Statement.RETURN_GENERATED_KEYS)) {
            pstmt.setString(1, this.actionId);
            pstmt.setString(2, this.stationId);
            pstmt.setString(3, this.qualification.toString());
            pstmt.setInt(4, this.numberOfQualified);
            int affectedRows = pstmt.executeUpdate();
            if (affectedRows > 0) {
                DatabaseUtil.print("Action request successfully: " + this);
            }
        } catch (SQLException ex) {
            DatabaseUtil.err("Error while storing action request: " + ex.getMessage());
        }
    }

    public String getActionId() {
        return actionId;
    }

    public void setActionId(String actionId) {
        this.actionId = actionId;
    }

    public String getStationId() {
        return stationId;
    }

    public void setStationId(String stationId) {
        this.stationId = stationId;
    }

    public QualificationName getQualification() {
        return qualification;
    }

    public void setQualification(QualificationName qualification) {
        this.qualification = qualification;
    }

    public int getNumberOfQualified() {
        return numberOfQualified;
    }

    public void setNumberOfQualified(int numberOfQualified) {
        this.numberOfQualified = numberOfQualified;
    }

    @Override
    public String toString() {
        return "ActionRequest{" +
                "actionId='" + actionId + '\'' +
                ", stationId='" + stationId + '\'' +
                ", qualification=" + qualification +
                ", numberOfQualified=" + numberOfQualified +
                '}';
    }
}
