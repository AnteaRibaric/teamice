package com.spring.teamice.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class Administrator extends User {
    Administrator(String userId, UserType userType, String username, String password, String firstName, String surname, String phoneNumber, String email, byte[] photo, boolean registered) {
        super(userId, userType, username, password, firstName, surname, phoneNumber, email, photo, registered);
    }

    Administrator(User user) {
        super(user.getUserId(), user.getUserType(), user.getUsername(), user.getPassword(), user.getFirstName(), user.getSurname(), user.getPhoneNumber(), user.getEmail(), user.getPhoto(), user.isRegistered());
    }

    public static void makeStation(String name, StationType type, Location location) {
        Station newStation = new Station(name, type, new Location(location.toString()));
        newStation.storeStation();
    }

    public static boolean makeStationLeader(String userId, String stationId) {
        String SQL = "UPDATE \"Savior\" SET \"lead\"= false WHERE \"stationId\" = ?"; //TODO rewrite

        try (Connection conn = DatabaseUtil.connectToDatabase();
             PreparedStatement pstmt = conn.prepareStatement(SQL)) {
            pstmt.setString(1, stationId);
            pstmt.executeUpdate();

            SQL = "UPDATE \"Savior\" SET \"lead\"= true WHERE \"userId\" = ?";
            PreparedStatement pstmt2 = conn.prepareStatement(SQL);
            pstmt2.setString(1, userId);
            pstmt2.executeUpdate();

        } catch (SQLException ex) {
            DatabaseUtil.err("Error making savior station leader:" + ex.getMessage());
            return false;
        }
        DatabaseUtil.print("Made station leader successfully");
        return true;
    }

    public static void addSaviorToStation(Savior savior, String stationId) {
        String SQL = "UPDATE \"Savior\" " +
                "SET " +
                " \"stationId\" = '" + stationId + "' " +
                "WHERE \"userId\" = '" + savior.getUserId() + "';";
        try (Connection conn = DatabaseUtil.connectToDatabase();
             PreparedStatement pstmt = conn.prepareStatement(SQL)) {
            pstmt.executeUpdate();
        } catch (SQLException ex) {
            DatabaseUtil.err("Error while adding savior to station: " + ex.getMessage());
        }
        DatabaseUtil.print("User added to station successfully");
    }

    public boolean updateStation(Station station) {
        if (station.getStationId() == null) {
            DatabaseUtil.print("Station id is null, unable to update station");
            return false;
        }
        DatabaseUtil.print("Station update: " + station);
        String SQL = "UPDATE \"User\" " +
                "SET " +
                " \"stationId\" = ?" +
                " , \"stationName\" = ?" +
                " , \"stationType\" = ?" +
                ",  \"location\" = ?" +
                "WHERE \"stationId\" = ?";
        try (Connection conn = DatabaseUtil.connectToDatabase();
             PreparedStatement pstmt = conn.prepareStatement(SQL)) {
            pstmt.setString(1, station.getStationId());
            pstmt.setString(2, station.getStationName());
            pstmt.setString(3, station.getStationType().toString());
            pstmt.setString(4, station.getLocation().toString());
            pstmt.setString(5, station.getStationId());
            pstmt.executeUpdate();
        } catch (SQLException ex) {
            DatabaseUtil.err("Error while updating station: " + ex.getMessage());
            return false;
        }

        DatabaseUtil.print("Station updated successfully");
        return true;
    }
}
