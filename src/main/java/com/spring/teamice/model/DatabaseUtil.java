package com.spring.teamice.model;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.*;


public class DatabaseUtil {
    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_BLUE = "\u001B[34m";
    private static Connection conn;

    public static Connection connectToDatabase() {
        String url = System.getenv("JDBC_DATABASE_URL");
        try {
            conn = DriverManager.getConnection(url);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return conn;
    }

    public static void createNewDatabase() {
        createDatabase();
    }


    public static void print(String text) {
        System.out.println(ANSI_BLUE + text + ANSI_RESET);
    }

    public static void err(String text) {
        System.out.println(ANSI_RED + text + ANSI_RESET);
    }

    public static Station getStationById(String stationId) {
        for (Station s : getAllStations()) {
            if (s.getStationId().equals(stationId)) {
                return s;
            }
        }
        DatabaseUtil.err("Couldn't find station in database for stationId = " + stationId);
        return null;
    }

    public static Savior getSaviorById(String userId) {
        for (Savior s : getAllSaviors()) {
            if (s.getUserId().equals(userId)) {
                return s;
            }
        }
        DatabaseUtil.err("Couldn't find savior in database for userId = " + userId);
        return null;
    }

    public static Action getActionById(String actionId) {
        for (Action a : getAllActions()) {
            if (a.getActionId().equals(actionId)) {
                return a;
            }
        }
        DatabaseUtil.err("Couldn't find savior in database for actionId = " + actionId);
        return null;
    }

    public static LeadSavior getLeadSaviorById(String userId) {
        for (LeadSavior ls : getAllLeadSaviors()) {
            if (ls.getUserId().equals(userId)) {
                return ls;
            }
        }
        DatabaseUtil.err("Couldn't find lead savior in database for userId = " + userId);
        return null;
    }

    public static Dispatcher getDispatcherById(String userId) {
        for (Dispatcher d : getAllDispatchers()) {
            if (d.getUserId().equals(userId)) {
                return d;
            }
        }
        DatabaseUtil.err("Couldn't find dispatcher in database for userId = " + userId);
        return null;
    }

    public static Administrator getAdministratorById(String userId) {
        for (Administrator a : getAllAdministrators()) {
            if (a.getUserId().equals(userId)) {
                return a;
            }
        }
        DatabaseUtil.err("Couldn't find administrator in database for userId = " + userId);
        return null;
    }


    public static List<Action> getAllActions() {
        String SQL = "SELECT * FROM \"Action\";";
        ArrayList<Action> listOfActions = new ArrayList<>();
        try (Connection conn = DatabaseUtil.connectToDatabase();
             Statement stmt = conn.createStatement();
             ResultSet rs = stmt.executeQuery(SQL)) {
            listOfActions = Action.actionMapper(rs);
            DatabaseUtil.print("Successfully retrieved actions");
        } catch (SQLException ex) {
            DatabaseUtil.err("Error while getting all actions: " + ex.getMessage());
        }
        return listOfActions;
    }

    public static ArrayList<User> getAllUsers() {
        String SQL = "SELECT * FROM \"User\" ORDER BY \"registered\",\"username\";";
        ArrayList<User> listOfUsers = new ArrayList<>();
        try (Connection conn = DatabaseUtil.connectToDatabase();
             Statement stmt = conn.createStatement();
             ResultSet rs = stmt.executeQuery(SQL)) {
            listOfUsers = User.userMapper(rs);
            DatabaseUtil.print("Successfully retrieved users");
        } catch (SQLException ex) {
            DatabaseUtil.err("Error while getting all users: " + ex.getMessage());
        }
        return listOfUsers;
    }

    public static ArrayList<Dispatcher> getAllDispatchers() {
        String SQL = "SELECT * FROM \"User\" NATURAL JOIN \"Dispatcher\" WHERE registered = true ORDER BY \"username\";";
        ArrayList<Dispatcher> listOfDispatchers = new ArrayList<>();
        try (Connection conn = DatabaseUtil.connectToDatabase();
             Statement stmt = conn.createStatement();
             ResultSet rs = stmt.executeQuery(SQL)) {
            for (User u : User.userMapper(rs))
                listOfDispatchers.add(new Dispatcher(u));
            DatabaseUtil.print("Successfully retrieved registered dispatchers");
        } catch (SQLException ex) {
            DatabaseUtil.err("Error when getting all registered dispatchers: " + ex.getMessage());
        }
        return listOfDispatchers;
    }

    public static ArrayList<Administrator> getAllAdministrators() {
        String SQL = "SELECT * FROM \"User\" NATURAL JOIN \"Dispatcher\" WHERE registered = true ORDER BY \"username\";";
        ArrayList<Administrator> listOfAdministrators = new ArrayList<>();
        try (Connection conn = DatabaseUtil.connectToDatabase();
             Statement stmt = conn.createStatement();
             ResultSet rs = stmt.executeQuery(SQL)) {
            for (User u : User.userMapper(rs))
                listOfAdministrators.add(new Administrator(u));
            DatabaseUtil.print("Successfully retrieved registered administrators");
        } catch (SQLException ex) {
            DatabaseUtil.err("Error when getting all registered administrators: " + ex.getMessage());
        }
        return listOfAdministrators;
    }

    public static ArrayList<Savior> getAllSaviors() {
        String SQL = "SELECT * FROM \"User\" NATURAL JOIN \"Savior\" WHERE registered = true ORDER BY \"username\";";
        ArrayList<Savior> listOfSaviors = new ArrayList<>();
        try (Connection conn = DatabaseUtil.connectToDatabase();
             Statement stmt = conn.createStatement();
             ResultSet rs = stmt.executeQuery(SQL)) {
            listOfSaviors = Savior.saviorMapper(rs);
            DatabaseUtil.print("Successfully retrieved registered saviors");
        } catch (SQLException ex) {
            DatabaseUtil.err("Error when getting all registered saviors: " + ex.getMessage());
        }
        return listOfSaviors;
    }

    public static ArrayList<LeadSavior> getAllLeadSaviors() {
        String SQL = "SELECT * FROM \"User\" NATURAL JOIN \"Savior\" WHERE registered = true AND lead = true ORDER BY \"username\";"; // TODO rewrite
        ArrayList<LeadSavior> listOfSaviors = new ArrayList<>();
        try (Connection conn = DatabaseUtil.connectToDatabase();
             Statement stmt = conn.createStatement();
             ResultSet rs = stmt.executeQuery(SQL)) {
            for (Savior s : Savior.saviorMapper(rs))
                listOfSaviors.add(new LeadSavior(s));
            DatabaseUtil.print("Successfully retrieved registered lead saviors");
        } catch (SQLException ex) {
            DatabaseUtil.err("Error when getting all lead saviors: " + ex.getMessage());
        }
        return listOfSaviors;
    }

    public static ArrayList<MapComment> getAllMapComments() {
        String SQL = "SELECT * FROM \"MapComment\";";
        ArrayList<MapComment> listOfMapComments = new ArrayList<>();
        try (Connection conn = DatabaseUtil.connectToDatabase();
             Statement stmt = conn.createStatement();
             ResultSet rs = stmt.executeQuery(SQL)) {
            listOfMapComments = MapComment.mapCommentMapper(rs);
            DatabaseUtil.print("Successfully retrieved stations");
        } catch (SQLException ex) {
            DatabaseUtil.err("Error when getting all stations: " + ex.getMessage());
        }
        return listOfMapComments;
    }

    public static ArrayList<WayPoint> getAllWayPoints() {
        String SQL = "SELECT * FROM \"WayPoint\";";
        ArrayList<WayPoint> listOfWayPoints = new ArrayList<>();
        try (Connection conn = DatabaseUtil.connectToDatabase();
             Statement stmt = conn.createStatement();
             ResultSet rs = stmt.executeQuery(SQL)) {
            listOfWayPoints = WayPoint.wayPointMapper(rs);
            DatabaseUtil.print("Successfully retrieved waypoints");
        } catch (SQLException ex) {
            DatabaseUtil.err("Error when getting all waypoints: " + ex.getMessage());
        }
        return listOfWayPoints;
    }

    public static List<WayPoint> getAllUserWayPoints(String userId) {
        ArrayList<WayPoint> wayPoints = new ArrayList<>();
        for (WayPoint wp : getAllWayPoints()) {
            if (userId.equals(wp.getUserId())) {
                wayPoints.add(wp);
            }
        }
        return wayPoints;
    }

    public static List<MapComment> getCurrentActionComments(String userId) {
        ActionInfo actionInfo = getSaviorById(userId).currentActionInfo();
        if (actionInfo == null) return new ArrayList<>();
        ArrayList<MapComment> comments = new ArrayList<>();
        for (MapComment comment : getAllMapComments()) {
            if (comment.getActionId().equals(actionInfo.getActionId())) {
                comments.add(comment);
            }
        }
        return comments;
    }

    public static ArrayList<Station> getAllStations() {
        String SQL = "SELECT * FROM \"Station\" ORDER BY \"stationName\";";
        ArrayList<Station> listOfStations = new ArrayList<>();
        try (Connection conn = DatabaseUtil.connectToDatabase();
             Statement stmt = conn.createStatement();
             ResultSet rs = stmt.executeQuery(SQL)) {
            listOfStations = Station.stationMapper(rs);
            DatabaseUtil.print("Successfully retrieved stations");
        } catch (SQLException ex) {
            DatabaseUtil.err("Error when getting all stations: " + ex.getMessage());
        }
        return listOfStations;
    }

    public static List<Task> getAllTasks() {
        String SQL = "SELECT * FROM \"Task\";";
        ArrayList<Task> listOfTasks = new ArrayList<>();
        try (Connection conn = DatabaseUtil.connectToDatabase();
             Statement stmt = conn.createStatement();
             ResultSet rs = stmt.executeQuery(SQL)) {
            listOfTasks = Task.taskMapper(rs);
            DatabaseUtil.print("Successfully retrieved tasks");
        } catch (SQLException ex) {
            DatabaseUtil.err("Error when getting all tasks: " + ex.getMessage());
        }
        return listOfTasks;
    }

    public static List<Task> getAllUsersTasks(String userId) {
        String SQL = "SELECT * FROM \"Tasks\" NATURAL JOIN \"Task\" WHERE \"userId\" = '" + userId + "';";
        ArrayList<Task> listOfTasks = new ArrayList<>();
        try (Connection conn = DatabaseUtil.connectToDatabase();
             Statement stmt = conn.createStatement();
             ResultSet rs = stmt.executeQuery(SQL)) {
            listOfTasks = Task.taskMapper(rs);
            DatabaseUtil.print("Successfully retrieved users tasks");
        } catch (SQLException ex) {
            DatabaseUtil.err("Error when getting all users tasks: " + ex.getMessage());
        }
        return listOfTasks;
    }

    public static void deleteAllUsers() {
        String SQL = "DELETE FROM \"User\" WHERE \"userType\" != 'ADMIN'";
        try (Connection conn = DatabaseUtil.connectToDatabase();
             PreparedStatement pstmt = conn.prepareStatement(SQL)) {

            pstmt.executeUpdate();
        } catch (SQLException ex) {
            DatabaseUtil.err("Error while deleting all users:" + ex.getMessage());
        }
        DatabaseUtil.print("All users deleted successfully");
    }

    public static void createBasicData() {
        new User(UUID.randomUUID().toString(), UserType.ADMIN, "admin", "12345678", "Zlatan", "Ibrahimović", "0980101010", "admin@gmail.com", getDefaultImage(), true).registerUser();
        new User(UUID.randomUUID().toString(), UserType.DISPATCHER, "dispečer", "12345678", "Majmun", "Mumo", "0980101010", "dispečer@gmail.com", getDefaultImage(), true).registerUser();
        new User(UUID.randomUUID().toString(), UserType.LEAD_SAVIOR, "gspasilac", "12345678", "John", "Travolta", "0980101010", "spasilac@gmail.com", getDefaultImage(), true).registerUser();
        new User(UUID.randomUUID().toString(), UserType.LEAD_SAVIOR, "gspasilac2", "12345678", "Michael", "Scofield", "0980101010", "spasilac@gmail.com", getDefaultImage(), true).registerUser();
        new User(UUID.randomUUID().toString(), UserType.LEAD_SAVIOR, "gspasilac3", "12345678", "Dominic", "Toretto", "0980101010", "spasilac@gmail.com", getDefaultImage(), true).registerUser();
        new User(UUID.randomUUID().toString(), UserType.SAVIOR, "spasilac", "12345678", "Elliot", "Alderson", "0980101010", "spasilac@gmail.com", getDefaultImage(), true).registerUser();
        new User(UUID.randomUUID().toString(), UserType.SAVIOR, "spasilac2", "12345678", "Ragnar", "Lothbrok", "0980101010", "spasilac@gmail.com", getDefaultImage(), true).registerUser();
        new User(UUID.randomUUID().toString(), UserType.SAVIOR, "spasilac3", "12345678", "Walter", "White", "0980101010", "spasilac@gmail.com", getDefaultImage(), true).registerUser();
        new User(UUID.randomUUID().toString(), UserType.SAVIOR, "spasilac4", "12345678", "Sheldon", "Cooper", "0980101010", "spasilac@gmail.com", getDefaultImage(), true).registerUser();
        new User(UUID.randomUUID().toString(), UserType.SAVIOR, "spasilac5", "12345678", "Michael", "Scott", "0980101010", "spasilac@gmail.com", getDefaultImage(), true).registerUser();
        new User(UUID.randomUUID().toString(), UserType.SAVIOR, "spasilac6", "12345678", "Rick", "Sanchez", "0980101010", "spasilac@gmail.com", getDefaultImage(), true).registerUser();
        new User(UUID.randomUUID().toString(), UserType.SAVIOR, "spasilac7", "12345678", "Lucifer", "Morningstar", "0980101010", "spasilac@gmail.com", getDefaultImage(), true).registerUser();
        new User(UUID.randomUUID().toString(), UserType.SAVIOR, "spasilac8", "12345678", "Tony", "Stark", "0980101010", "spasilac@gmail.com", getDefaultImage(), true).registerUser();
        new User(UUID.randomUUID().toString(), UserType.SAVIOR, "spasilac9", "12345678", "Momčilo", "Bajagić", "0980101010", "spasilac@gmail.com", getDefaultImage(), true).registerUser();
        new User(UUID.randomUUID().toString(), UserType.SAVIOR, "spasilac10", "12345678", "Tom", "Hardy", "0980101010", "spasilac@gmail.com", getDefaultImage(), true).registerUser();
        for (User u : getAllUsers()) {
            User.acceptUser(u.getUserId());
        }
        new User(UUID.randomUUID().toString(), UserType.SAVIOR, UUID.randomUUID().toString(), "12345678", "Majmun", "Mumo", "0980101010", "spasilac2@gmail.com", getDefaultImage(), false).registerUser();
        new User(UUID.randomUUID().toString(), UserType.DISPATCHER, UUID.randomUUID().toString(), "12345678", "Majmun", "Mumo", "0980101010", "spasilac2@gmail.com", getDefaultImage(), false).registerUser();
        new Station("Plitvice", StationType.PERMANENT_STATION, new Location("44.8637$15.5759")).storeStation();
        new Station("Učka", StationType.PERMANENT_STATION, new Location("45.2749$14.1751")).storeStation();
        new Station("Medvednica", StationType.PERMANENT_STATION, new Location("45.9091$15.9906")).storeStation();
        new Station("Papuk", StationType.PERMANENT_STATION, new Location("45.5063$17.7319")).storeStation();
        new Station("privremena", StationType.TEMPORARY_STATION, new Location("46.0313$16.6003")).storeStation();
        new Station("šator", StationType.TEMPORARY_STATION, new Location("45.4138$15.5786")).storeStation();
        new Station("Gospić", StationType.PERMANENT_STATION, new Location("44.5298$15.3699")).storeStation();
        new Station("Biokovo", StationType.PERMANENT_STATION, new Location("43.3232$17.0618")).storeStation();
        Dispatcher.startAction("Spašavanje vojnika Ryana", new Location("45.9011$15.9344"));
        Dispatcher.startAction("Pronađi Walda", new Location("45.1052$15.4948"));
        Dispatcher.startAction("Storm Capital", new Location("45.8164$15.9737"));
        generateBasicData();
    }

    private static void generateBasicData() {
        Map<String, List<String>> stationMap = new HashMap<>();
        stationMap.put("Medvednica", new ArrayList<>(Arrays.asList("gspasilac", "spasilac2", "spasilac5")));
        stationMap.put("Plitvice", new ArrayList<>(Arrays.asList("gspasilac3", "spasilac3")));
        stationMap.put("Papuk", new ArrayList<>(Arrays.asList("gspasilac2", "spasilac", "spasilac4")));
        for (String key : stationMap.keySet()) {
            System.out.println(key);
            Station station = getStationByName(key);
            for (String un : stationMap.get(key)) {
                for (Savior s : getAllSaviors()) {
                    if (s.getUsername().equals(un)) {
                        Administrator.addSaviorToStation(s, station.getStationId());
                    }
                }
            }
        }
        addSaviorQualifications();
    }

    private static void addSaviorQualifications() {
        Map<String, List<QualificationName>> qualyMap = new HashMap<>();
        qualyMap.put("gspasilac", new ArrayList<>(Arrays.asList(QualificationName.BOAT, QualificationName.HELICOPTER, QualificationName.DRONE)));
        qualyMap.put("gpsasilac2", new ArrayList<>(Arrays.asList(QualificationName.DRONE, QualificationName.BICYCLE, QualificationName.WITH_DOG)));
        qualyMap.put("gspaslac3", new ArrayList<>(Arrays.asList(QualificationName.CAR, QualificationName.ON_FOOT, QualificationName.HELICOPTER)));
        qualyMap.put("spasilac", new ArrayList<>(Arrays.asList(QualificationName.WITH_DOG, QualificationName.HELICOPTER, QualificationName.BOAT)));
        qualyMap.put("spasilac2", new ArrayList<>(Arrays.asList(QualificationName.WITH_DOG, QualificationName.DRONE, QualificationName.ON_FOOT, QualificationName.BICYCLE)));
        qualyMap.put("spasilac3", new ArrayList<>(Arrays.asList(QualificationName.DRONE, QualificationName.CAR, QualificationName.ON_FOOT)));
        qualyMap.put("spasilac4", new ArrayList<>(Arrays.asList(QualificationName.WITH_DOG, QualificationName.BICYCLE, QualificationName.CAR, QualificationName.ON_FOOT)));
        qualyMap.put("spasilac5", new ArrayList<>(Arrays.asList(QualificationName.WITH_DOG, QualificationName.BICYCLE, QualificationName.CAR, QualificationName.ON_FOOT, QualificationName.DRONE, QualificationName.HELICOPTER, QualificationName.BOAT)));
        for (String key : qualyMap.keySet()) {
            ArrayList<Savior> saviors = getAllSaviors();
            for (Savior s : saviors) {
                if (key.equals(s.getUsername())) {
                    for (QualificationName q : qualyMap.get(key)) {
                        LeadSavior.addSaviorQualification(s.getUserId(), q);
                    }
                }
            }
        }
    }

    private static Station getStationByName(String st) {
        for (Station s : getAllStations()) {
            if (s.getStationName().equals(st)) {
                return s;
            }
        }
        return new Station();
    }

    public static void createDatabase() {
        String SQL = "create table \"User\"\n" +
                "(\n" +
                "    \"userId\"      varchar(50) not null\n" +
                "        constraint \"PK_user\"\n" +
                "            primary key,\n" +
                "    username      varchar(50) not null\n" +
                "        constraint \"User_username_key\"\n" +
                "            unique,\n" +
                "    password      varchar(50) not null,\n" +
                "    \"firstName\"   varchar(50) not null,\n" +
                "    \"userType\"    varchar(50) not null,\n" +
                "    \"lastName\"    varchar(50) not null,\n" +
                "    \"phoneNumber\" varchar(15) not null,\n" +
                "    email         varchar(50) not null,\n" +
                "    registered    boolean     not null,\n" +
                "    \"userPhoto\"   bytea       not null\n" +
                ");\n" +
                "\n" +
                "create table \"Station\"\n" +
                "(\n" +
                "    \"stationId\"   varchar(50) not null\n" +
                "        constraint \"PK_station\"\n" +
                "            primary key,\n" +
                "    \"stationName\" varchar(50),\n" +
                "    \"stationType\" varchar(20),\n" +
                "    location      varchar(50)\n" +
                ");\n" +
                "\n" +
                "create table \"Savior\"\n" +
                "(\n" +
                "    \"userId\"    varchar(50) not null\n" +
                "        constraint \"PK_savior\"\n" +
                "            primary key\n" +
                "        constraint \"FK_85\"\n" +
                "            references \"User\"\n" +
                "            on update cascade on delete cascade,\n" +
                "    available   boolean,\n" +
                "    location    varchar(50),\n" +
                "    \"stationId\" varchar(50)\n" +
                "        constraint \"FK_161\"\n" +
                "            references \"Station\"\n" +
                "            on update cascade on delete cascade\n" +
                ");\n" +
                "\n" +
                "\n" +
                "create index \"fkIdx_161\"\n" +
                "    on \"Savior\" (\"stationId\");\n" +
                "\n" +
                "create index \"fkIdx_85\"\n" +
                "    on \"Savior\" (\"userId\");\n" +
                "\n" +
                "create table \"Action\"\n" +
                "(\n" +
                "    \"actionId\"  varchar(50) not null\n" +
                "        constraint \"PK_action\"\n" +
                "            primary key,\n" +
                "    description varchar(50),\n" +
                "    location    varchar(50) not null,\n" +
                "    active      boolean\n" +
                ");\n" +
                "\n" +
                "\n" +
                "create table \"ActionRequests\"\n" +
                "(\n" +
                "    \"actionId\"          varchar(50) not null\n" +
                "        constraint \"FK_130\"\n" +
                "            references \"Action\"\n" +
                "            on update cascade on delete cascade,\n" +
                "    \"stationId\"         varchar(50) not null\n" +
                "        constraint \"FK_125\"\n" +
                "            references \"Station\"\n" +
                "            on update cascade on delete cascade,\n" +
                "    \"qualificationId\"   varchar(50) not null,\n" +
                "    \"numberOfQualified\" numeric,\n" +
                "    constraint \"PK_actionrequests\"\n" +
                "        primary key (\"actionId\", \"stationId\", \"qualificationId\")\n" +
                ");\n" +
                "\n" +
                "\n" +
                "create index \"fkIdx_125\"\n" +
                "    on \"ActionRequests\" (\"stationId\");\n" +
                "\n" +
                "create index \"fkIdx_130\"\n" +
                "    on \"ActionRequests\" (\"actionId\");\n" +
                "\n" +
                "create table \"Admin\"\n" +
                "(\n" +
                "    \"userId\" varchar(50) not null\n" +
                "        constraint \"PK_admin\"\n" +
                "            primary key\n" +
                "        constraint \"FK_80\"\n" +
                "            references \"User\"\n" +
                "            on update cascade on delete cascade\n" +
                ");\n" +
                "\n" +
                "\n" +
                "create index \"fkIdx_80\"\n" +
                "    on \"Admin\" (\"userId\");\n" +
                "\n" +
                "create table \"Dispatcher\"\n" +
                "(\n" +
                "    \"userId\" varchar(50) not null\n" +
                "        constraint \"PK_dispatcher\"\n" +
                "            primary key\n" +
                "        constraint \"FK_76\"\n" +
                "            references \"User\"\n" +
                "            on update cascade on delete cascade\n" +
                ");\n" +
                "\n" +
                "\n" +
                "create index \"fkIdx_76\"\n" +
                "    on \"Dispatcher\" (\"userId\");\n" +
                "\n" +
                "create table \"MapComment\"\n" +
                "(\n" +
                "    \"actionId\"    varchar(50) not null\n" +
                "        constraint \"FK_203\"\n" +
                "            references \"Action\"\n" +
                "            on update cascade on delete cascade,\n" +
                "    \"commentId\"   varchar(50) not null,\n" +
                "    \"commentText\" varchar(1000),\n" +
                "    location      varchar(50) not null,\n" +
                "    constraint \"PK_mapcomment\"\n" +
                "        primary key (\"commentId\", \"actionId\")\n" +
                ");\n" +
                "\n" +
                "\n" +
                "create table \"Qualification\"\n" +
                "(\n" +
                "    \"qualificationId\"       varchar(50) not null\n" +
                "        constraint \"PK_qualification\"\n" +
                "            primary key,\n" +
                "    \"qualificationName\"     varchar(20) not null,\n" +
                "    \"qualificationCoverage\" varchar(50) not null\n" +
                ");\n" +
                "\n" +
                "\n" +
                "create table \"hasQualification\"\n" +
                "(\n" +
                "    \"userId\"          varchar(50) not null\n" +
                "        constraint \"FK_59\"\n" +
                "            references \"Savior\"\n" +
                "            on update cascade on delete cascade,\n" +
                "    \"qualificationId\" varchar(50) not null\n" +
                "        constraint \"FK_204\"\n" +
                "            references \"Qualification\"\n" +
                "            on update cascade on delete cascade,\n" +
                "    constraint \"PK_hasqualification\"\n" +
                "        primary key (\"userId\", \"qualificationId\")\n" +
                ");\n" +
                "\n" +
                "create index \"fkIdx_60\"\n" +
                "    on \"hasQualification\" (\"userId\");\n" +
                "\n" +
                "create table \"SaviorsInAction\"\n" +
                "(\n" +
                "    \"actionId\"        varchar(50) not null\n" +
                "        constraint \"FK_164\"\n" +
                "            references \"Action\"\n" +
                "            on update cascade on delete cascade,\n" +
                "    \"userId\"          varchar(50) not null\n" +
                "        constraint \"FK_167\"\n" +
                "            references \"Savior\"\n" +
                "            on update cascade on delete cascade,\n" +
                "    \"qualificationId\" varchar(50) not null\n" +
                "        constraint \"FK_202\"\n" +
                "            references \"Qualification\"\n" +
                "            on update cascade on delete cascade,\n" +
                "    constraint \"PK_saviorsinaction\"\n" +
                "        primary key (\"actionId\", \"userId\", \"qualificationId\")\n" +
                ");\n" +
                "\n" +
                "\n" +
                "create index \"fkIdx_164\"\n" +
                "    on \"SaviorsInAction\" (\"actionId\");\n" +
                "\n" +
                "create index \"fkIdx_167\"\n" +
                "    on \"SaviorsInAction\" (\"userId\");\n" +
                "\n" +
                "create table \"Task\"\n" +
                "(\n" +
                "    \"taskId\"   varchar(50) not null\n" +
                "        constraint \"PK_task\"\n" +
                "            primary key,\n" +
                "    location   varchar(50) not null,\n" +
                "    \"taskType\" varchar(50) not null,\n" +
                "    comment    varchar(1000)\n" +
                ");\n" +
                "\n" +
                "\n" +
                "create index \"fkIdx_135\"\n" +
                "    on \"Task\" (location);\n" +
                "\n" +
                "create table \"Tasks\"\n" +
                "(\n" +
                "    \"actionId\" varchar(50) not null\n" +
                "        constraint \"FK_142\"\n" +
                "            references \"Action\"\n" +
                "            on update cascade on delete cascade,\n" +
                "    \"taskId\"   varchar(50) not null\n" +
                "        constraint \"FK_145\"\n" +
                "            references \"Task\"\n" +
                "            on update cascade on delete cascade,\n" +
                "    \"userId\"   varchar(50) not null\n" +
                "        constraint \"FK_148\"\n" +
                "            references \"Savior\"\n" +
                "            on update cascade on delete cascade,\n" +
                "    constraint \"PK_tasks\"\n" +
                "        primary key (\"actionId\", \"taskId\", \"userId\")\n" +
                ");\n" +
                "\n" +
                "\n" +
                "create index \"fkIdx_142\"\n" +
                "    on \"Tasks\" (\"actionId\");\n" +
                "\n" +
                "create index \"fkIdx_145\"\n" +
                "    on \"Tasks\" (\"taskId\");\n" +
                "\n" +
                "create index \"fkIdx_148\"\n" +
                "    on \"Tasks\" (\"userId\");\n" +
                "\n" +
                "create table \"HeatTrace\"\n" +
                "(\n" +
                "    \"userId\"  varchar(50)         not null\n" +
                "        constraint \"FK_201\"\n" +
                "            references \"Savior\"\n" +
                "            on update cascade on delete cascade,\n" +
                "    x         real,\n" +
                "    y         real,\n" +
                "    opacity   varchar(50),\n" +
                "    timestamp time with time zone not null,\n" +
                "    radius    double precision,\n" +
                "    constraint \"PK_heattrace\"\n" +
                "        primary key (timestamp, \"userId\")\n" +
                ");\n" +
                "\n" +
                "create table \"WayPoint\"\n" +
                "(\n" +
                "    \"userId\"     varchar(50) not null\n" +
                "        constraint \"FK_200\"\n" +
                "            references \"User\"\n" +
                "            on update cascade on delete cascade,\n" +
                "    \"wayPointId\" varchar(50) not null,\n" +
                "    location     varchar(50),\n" +
                "    constraint \"PK_waypoint\"\n" +
                "        primary key (\"wayPointId\", \"userId\")\n" +
                ");\n" +
                "\n";
        System.out.println(SQL);
        try (Connection conn = DatabaseUtil.connectToDatabase();
             Statement stmt = conn.createStatement()) {
            stmt.executeUpdate(SQL);
        } catch (SQLException ex) {
            DatabaseUtil.err("Error while creating database:" + ex.getMessage());
        }
        SaviorQualification.createQualifications();
        createBasicData();
    }

    public static byte[] getDefaultImage() {
        byte[] bytes;
        byte[] bytesPhoto = new byte[0];

        InputStream is = DatabaseUtil.class.getClassLoader().getResourceAsStream("static/images/cvjetic.png");

        try {
            bytes = is.readAllBytes();
            is.close();

            String encodedString = Base64.getEncoder().encodeToString(bytes);
            bytesPhoto = encodedString.getBytes();

        } catch (IOException e) {
            System.out.println("Error while loading resource");
        }
        return bytesPhoto;
    }
}
