package com.spring.teamice.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.UUID;

public class Dispatcher extends User {

    Dispatcher(String userId, UserType userType, String username, String password, String firstName, String surname, String phoneNumber, String email, byte[] photo, boolean registered) {
        super(userId, userType, username, password, firstName, surname, phoneNumber, email, photo, registered);
    }

    Dispatcher(User user) {
        super(user.getUserId(), user.getUserType(), user.getUsername(), user.getPassword(), user.getFirstName(), user.getSurname(), user.getPhoneNumber(), user.getEmail(), user.getPhoto(), user.isRegistered());
    }

    public static void addMapComment(String actionId, String text, Location location) {
        MapComment mapComment = new MapComment(actionId, text, location);
        mapComment.storeMapComment();
    }

    public static void startAction(String description, Location location) {
        Action newAction = new Action(description, location, true);
        newAction.storeAction();
    }

    public static void closeAction(String actionId) {
        String SQL = "UPDATE \"Action\" SET \"active\"= false WHERE \"actionId\" = ?";
        try (Connection conn = DatabaseUtil.connectToDatabase();
             PreparedStatement pstmt = conn.prepareStatement(SQL)) {
            pstmt.setString(1, actionId);
            pstmt.executeUpdate();
        } catch (SQLException ex) {
            DatabaseUtil.print("Error while closing action:" + ex.getMessage());
            return;
        }
        Task.deleteActionTasks(actionId);
        ActionRequest.deleteActionRequests(actionId);
        Savior.makeAvailable(DatabaseUtil.getActionById(actionId).fetchSaviorsInAction());
        Action.removeAllActionSaviors(actionId);
        DatabaseUtil.print("Action closed successfully");
    }

    public static void sendActionRequest(ActionRequest actionRequest) {
        actionRequest.storeRequest();
    }

    public static void assignSaviorTask(String userId, String actionId, String location, TaskType taskType, String comment) {
        String SQL = "INSERT INTO \"Task\" VALUES(?,?,?,?);";
        int affectedRows = 0;
        String taskId = UUID.randomUUID().toString();
        try (Connection conn = DatabaseUtil.connectToDatabase();
             PreparedStatement pstmt = conn.prepareStatement(SQL,
                     Statement.RETURN_GENERATED_KEYS)) {
            pstmt.setString(1, taskId);
            pstmt.setString(2, location);
            pstmt.setString(3, taskType.toString());
            pstmt.setString(4, comment);
            affectedRows = pstmt.executeUpdate();
            String insertSQL = "INSERT INTO \"Tasks\" VALUES (?,?,?)";
            PreparedStatement pstmt2 = conn.prepareStatement(insertSQL);
            pstmt2.setString(1, actionId);
            pstmt2.setString(2, taskId);
            pstmt2.setString(3, userId);
            pstmt2.executeUpdate();
        } catch (SQLException ex) {
            DatabaseUtil.err("Error while assigning savior task: " + ex.getMessage());
        }
        if (affectedRows > 0) {
            DatabaseUtil.print("Assigned savior task successfully");
        }
    }

    public static void removeSaviorFromAction(String userId) {
        Savior savior = DatabaseUtil.getSaviorById(userId);
        assert savior != null;
        savior.updateAvailability(true);
        String actionId = savior.getCurrentActionId();

        String SQL = "DELETE FROM \"SaviorsInAction\" WHERE \"userId\" = ? AND \"actionId\" = ? ";

        try (Connection conn = DatabaseUtil.connectToDatabase();
             PreparedStatement pstmt = conn.prepareStatement(SQL)) {
            pstmt.setString(1, userId);
            pstmt.setString(2, actionId);
            pstmt.executeUpdate();
        } catch (SQLException ex) {
            DatabaseUtil.print("Error while removing savior from action:" + ex.getMessage());
        }
        DatabaseUtil.print("Savior removed from action successfully");
    }
}
