package com.spring.teamice.model;

import java.sql.*;
import java.time.Instant;
import java.util.*;

public class HeatTrace {
    private String userId;
    private Location location;
    private Instant timestamp;
    private float opacity;
    private float radius;
    private List<Float> heatData;

    public HeatTrace(String userId, Location location, Instant timestamp, float opacity, float radius) {
        this.userId = userId;
        this.location = location;
        this.timestamp = timestamp;
        this.opacity = opacity;
        this.radius = radius;
        heatData = new ArrayList<>(Arrays.asList(location.getX(), location.getY(), opacity / 100));
    }

    public HeatTrace(String userId, Location location, float opacity, float radius) {
        this.userId = userId;
        this.location = location;
        this.timestamp = Instant.now();
        this.opacity = opacity;
        this.radius = radius;
        heatData = new ArrayList<>(Arrays.asList(location.getX(), location.getY(), opacity / 100));
    }

    public static boolean storeHeatTrace(HeatTrace heatTrace) {
        String SQLQuery = "" +
                "INSERT INTO \"HeatTrace\"" +
                "VALUES (?,?,?,?,?,?)";
        try (Connection conn = DatabaseUtil.connectToDatabase();
             PreparedStatement pstmt = conn.prepareStatement(SQLQuery,
                     Statement.RETURN_GENERATED_KEYS)) {
            pstmt.setString(1, heatTrace.userId);
            pstmt.setDouble(2, heatTrace.location.getX());
            pstmt.setDouble(3, heatTrace.location.getY());
            pstmt.setDouble(4, heatTrace.opacity);
            pstmt.setTimestamp(5, Timestamp.from(heatTrace.timestamp));
            pstmt.setDouble(6, heatTrace.radius);

            int affectedRows = pstmt.executeUpdate();
            if (affectedRows > 0) {
                DatabaseUtil.print("HeatTrace stored successfully: " + heatTrace);
            }
        } catch (SQLException ex) {
            DatabaseUtil.err("Error while storing heatTrace: " + ex.getMessage());
            return false;
        }
        return true;
    }

    public static void deleteOldHeatTraces() {
        String SQL = "DELETE FROM \"HeatTrace\" WHERE \"timestamp\" < current_time - INTERVAL '2 HOURS';";
        int rowsAffected;
        try (Connection conn = DatabaseUtil.connectToDatabase();
             PreparedStatement pstmt = conn.prepareStatement(SQL, Statement.RETURN_GENERATED_KEYS)) {
            rowsAffected = pstmt.executeUpdate();
            DatabaseUtil.print("HeatTrace deleted successfully, deleted " + rowsAffected + " heattraces");

        } catch (SQLException ex) {
            DatabaseUtil.err("Error while deleting heatTrace:" + ex.getMessage());
        }
    }

    //    public static List<List<Float>> getAllHeatTraces() {
//        deleteOldHeatTraces();
//        String SQL = "SELECT * FROM \"HeatTrace\";";
//        List<List<Float>> listOfLocations = new ArrayList<>();
//        try (Connection conn = DatabaseUtil.connectToDatabase();
//             Statement stmt = conn.createStatement();
//             ResultSet rs = stmt.executeQuery(SQL)) {
//
//            while (rs.next()) {
//                List<Float> list = new ArrayList<>();
//                Float x = rs.getFloat("x");
//                Float y = rs.getFloat("y");
//                Float opacity = rs.getFloat("opacity");
//                list.add(x);
//                list.add(y);
//                list.add(opacity);
//                listOfLocations.add(list);
//            }
//
//        } catch (SQLException ex) {
//            DatabaseUtil.err("Error while getting heat map traces: " + ex.getMessage());
//        }
//        DatabaseUtil.print("Got heatmap traces successfully");
//        return listOfLocations;
//    }
    public static List<HeatTrace> getAllHeatTraces() {
        deleteOldHeatTraces();
        String SQL = "SELECT * FROM \"HeatTrace\";";
        List<HeatTrace> listOfHeatTraces = new ArrayList<>();
        try (Connection conn = DatabaseUtil.connectToDatabase();
             Statement stmt = conn.createStatement();
             ResultSet rs = stmt.executeQuery(SQL)) {

            while (rs.next()) {
                HeatTrace heatTrace = new HeatTrace(rs.getString("userId"), new Location(rs.getFloat("x"), rs.getFloat("y")), rs.getTimestamp("timestamp").toInstant(), rs.getFloat("opacity"), rs.getFloat("radius"));
                listOfHeatTraces.add(heatTrace);
            }

        } catch (SQLException ex) {
            DatabaseUtil.err("Error while getting heat map traces: " + ex.getMessage());
        }
        DatabaseUtil.print("Got heatmap traces successfully");
        return listOfHeatTraces;
    }

    public static Map<Float, List<List<Float>>> generateHeatData(List<HeatTrace> listOfHeatTraces) {
        Map<Float, List<List<Float>>> data = new HashMap<>();
        for (HeatTrace trace : listOfHeatTraces) {
            if (!data.containsKey(trace.radius)) {
                data.put(trace.radius, new ArrayList<>(Collections.singletonList(trace.heatData)));
            } else {
                data.get(trace.radius).add(trace.heatData);
            }
        }
        return data;
//        ObjectMapper objectMapper = new ObjectMapper();
//        try {
//            return objectMapper.writeValueAsString(data);
//        }catch (JsonProcessingException e){
//            DatabaseUtil.err("Error while generating trace data");
//            e.printStackTrace();
//            return null;
//        }
    }

    public static List<List<Float>> getAllHeatTracesAndQualification(Float radius) {
        deleteOldHeatTraces();
        String SQL = "SELECT * FROM \"HeatTrace\" WHERE \"radius\" = '" + radius + "'";

        List<List<Float>> listOfLocationsQualification = new ArrayList<>();
        try (Connection conn = DatabaseUtil.connectToDatabase();
             Statement stmt = conn.createStatement();
             ResultSet rs = stmt.executeQuery(SQL)) {
            while (rs.next()) {
                List<Float> list = new ArrayList<>();
                Float x = rs.getFloat("x");
                Float y = rs.getFloat("y");
                Float opacity = rs.getFloat("opacity");
                list.add(x);
                list.add(y);
                list.add(opacity);
                listOfLocationsQualification.add(list);
            }

        } catch (SQLException ex) {
            DatabaseUtil.err("Error while getting heat map traces: " + ex.getMessage());
        }
        DatabaseUtil.print("Got heatmap traces successfully");
        return listOfLocationsQualification;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public void setTimestamp(Instant timestamp) {
        this.timestamp = timestamp;
    }

    public void setOpacity(float opacity) {
        this.opacity = opacity;
    }

    public void setRadius(float radius) {
        this.radius = radius;
    }

    public List<Float> getHeatData() {
        return heatData;
    }

    public void setHeatData(List<Float> heatData) {
        this.heatData = heatData;
    }

    @Override
    public String toString() {
        return "HeatTrace{" +
                "userId='" + userId + '\'' +
                ", location=" + location +
                ", timestamp=" + timestamp +
                ", opacity=" + opacity +
                ", radius=" + radius +
                '}';
    }
}
