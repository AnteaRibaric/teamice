package com.spring.teamice.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class LeadSavior extends Savior {
    LeadSavior(String userId, UserType userType, String username, String password, String firstName, String surname, String phoneNumber, String email, byte[] photo, boolean registered) {
        super(userId, userType, username, password, firstName, surname, phoneNumber, email, photo, registered);
    }

    LeadSavior(User user) {
        super(user.getUserId(), user.getUserType(), user.getUsername(), user.getPassword(), user.getFirstName(), user.getSurname(), user.getPhoneNumber(), user.getEmail(), user.getPhoto(), user.isRegistered());
    }

    public LeadSavior(Savior savior) {
        super(savior.getUserId(), savior.getUserType(), savior.getUsername(), savior.getPassword(), savior.getFirstName(), savior.getSurname(), savior.getPhoneNumber(), savior.getEmail(), savior.getPhoto(), savior.isRegistered());
    }

    public static void addSaviorQualification(String userId, QualificationName qualification) {
        String SQL = "INSERT INTO \"hasQualification\" VALUES (?,?)";
        try (Connection conn = DatabaseUtil.connectToDatabase();
             PreparedStatement pstmt = conn.prepareStatement(SQL)) {
            pstmt.setString(1, userId);
            pstmt.setString(2, qualification.toString());
            pstmt.executeUpdate();
            DatabaseUtil.print("Qualifications updated successfully");
        } catch (SQLException ex) {
            DatabaseUtil.err("Error while updating savior qualifications:" + ex.getMessage());
        }
    }

    public static void removeAllSaviorQualifications(String userId) {
        String SQL = "DELETE FROM \"hasQualification\" WHERE \"userId\" = " + userId;
        try (Connection conn = DatabaseUtil.connectToDatabase();
             PreparedStatement pstmt = conn.prepareStatement(SQL)) {
            pstmt.setString(1, userId);
            pstmt.executeUpdate();
            DatabaseUtil.print("Saviors qualifications deleted successfully");
        } catch (SQLException ex) {
            DatabaseUtil.err("Error while deleting savior qualifications:" + ex.getMessage());
        }
    }

}
