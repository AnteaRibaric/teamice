package com.spring.teamice.model;

public class Location {
    private float x;
    private float y;

    public Location(String location) {
        if (location != null) {
            String[] list = location.split("\\$", -1);
            this.x = Float.parseFloat(list[0]);
            this.y = Float.parseFloat(list[1]);
        } else {
            this.x = 0;
            this.y = 0;
        }
    }

    public Location(float x, float y) {
        this.x = x;
        this.y = y;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    @Override
    public String toString() {
        return x + "$" + y;
    }
}
