package com.spring.teamice.model;

public class LoginStatus {
    private String userId;
    private String message;
    private Boolean successfullLogin;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getSuccessfullLogin() {
        return successfullLogin;
    }

    public void setSuccessfullLogin(Boolean successfullLogin) {
        this.successfullLogin = successfullLogin;
    }

    @Override
    public String toString() {
        return "LoginStatus{" +
                "userId='" + userId + '\'' +
                ", message='" + message + '\'' +
                ", successfullLogin=" + successfullLogin +
                '}';
    }
}
