package com.spring.teamice.model;

import java.sql.*;
import java.util.ArrayList;
import java.util.UUID;

public class MapComment {
    private String actionId;
    private String commentId;
    private String commentText;
    private Location location;

    public MapComment(String actionId, String text, Location location) {
        this.actionId = actionId;
        commentId = UUID.randomUUID().toString();
        commentText = text;
        this.location = location;
    }

    MapComment(String actionId, String commentId, String commentText, Location location) {
        this.actionId = actionId;
        this.commentId = commentId;
        this.commentText = commentText;
        this.location = location;
    }

    public static ArrayList<MapComment> mapCommentMapper(ResultSet rs) throws SQLException {
        ArrayList<MapComment> listOfMapComments = new ArrayList<>();
        while (rs.next()) {
            MapComment mapComment = new MapComment(rs.getString("actionId"), rs.getString("commentId"), rs.getString("commentText"), new Location((rs.getString("location"))));
            listOfMapComments.add(mapComment);
        }
        return listOfMapComments;
    }

    public void storeMapComment() {
        String SQLQuery = "" +
                "INSERT INTO \"MapComment\"" +
                "VALUES (?,?,?,?)";
        try (Connection conn = DatabaseUtil.connectToDatabase();
             PreparedStatement pstmt = conn.prepareStatement(SQLQuery,
                     Statement.RETURN_GENERATED_KEYS)) {
            pstmt.setString(1, this.actionId);
            pstmt.setString(2, this.commentId);
            pstmt.setString(3, this.commentText);
            pstmt.setString(4, this.location.toString());

            int affectedRows = pstmt.executeUpdate();
            if (affectedRows > 0) {
                DatabaseUtil.print("Station stored successfully: " + this);
            }
        } catch (SQLException ex) {
            DatabaseUtil.err("Error while storing station: " + ex.getMessage());
        }
    }

    public void deleteComment() {
        String SQL = "DELETE FROM \"MapComment\" WHERE \"commentId\" = ?";

        try (Connection conn = DatabaseUtil.connectToDatabase();
             PreparedStatement pstmt = conn.prepareStatement(SQL)) {

            pstmt.setString(1, commentId);
            pstmt.executeUpdate();
        } catch (SQLException ex) {
            DatabaseUtil.print("Error while deleting comment:" + ex.getMessage());
        }
        DatabaseUtil.print("Comment deleted successfully");
    }

    public String getActionId() {
        return actionId;
    }

    public void setActionId(String actionId) {
        this.actionId = actionId;
    }

    public String getCommentId() {
        return commentId;
    }

    public void setCommentId(String commentId) {
        this.commentId = commentId;
    }

    public String getCommentText() {
        return commentText;
    }

    public void setCommentText(String commentText) {
        this.commentText = commentText;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    @Override
    public String toString() {
        return "MapComment{" +
                "actionId='" + actionId + '\'' +
                ", commentId='" + commentId + '\'' +
                ", commentText='" + commentText + '\'' +
                ", location=" + location +
                '}';
    }

}
