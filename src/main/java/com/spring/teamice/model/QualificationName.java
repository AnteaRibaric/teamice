package com.spring.teamice.model;

public enum QualificationName {
    CAR("Automobil"),
    BICYCLE("Bicikl"),
    ON_FOOT("Pješak"),
    WITH_DOG("Sa psom"),
    DRONE("Dron"),
    HELICOPTER("Helikopter"),
    BOAT("Brod");

    private final String name;

    QualificationName(String name) {
        this.name = name;
    }

    public static QualificationName toEnum(String qualificationName) {
        if (QualificationName.CAR.toString().equals(qualificationName)) return QualificationName.CAR;
        else if (QualificationName.BICYCLE.toString().equals(qualificationName)) return QualificationName.BICYCLE;
        else if (QualificationName.ON_FOOT.toString().equals(qualificationName)) return QualificationName.ON_FOOT;
        else if (QualificationName.WITH_DOG.toString().equals(qualificationName)) return QualificationName.WITH_DOG;
        else if (QualificationName.DRONE.toString().equals(qualificationName)) return QualificationName.DRONE;
        else if (QualificationName.HELICOPTER.toString().equals(qualificationName)) return QualificationName.HELICOPTER;
        else if (QualificationName.BOAT.toString().equals(qualificationName)) return QualificationName.BOAT;
        else return null;
    }

    public int coverage() {
        switch (this) {
            case ON_FOOT -> {
                return 10;
            }
            case WITH_DOG -> {
                return 12;
            }
            case BICYCLE -> {
                return 15;
            }
            case CAR -> {
                return 25;
            }
            case DRONE -> {
                return 35;
            }
            case BOAT -> {
                return 50;
            }
            case HELICOPTER -> {
                return 80;
            }
            default -> {
                return 0;
            }
        }
    }


    public String getName() {
        return name;
    }
}
