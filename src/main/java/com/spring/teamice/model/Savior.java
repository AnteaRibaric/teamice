package com.spring.teamice.model;


import java.sql.*;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;

public class Savior extends User {
    private String stationId;
    private Location location;
    private Boolean available;
    private List<SaviorQualification> qualifications;
    private List<Task> tasks;

    Savior(String userId, UserType userType, String username, String password, String firstName, String surname, String phoneNumber, String email, byte[] photo, boolean registered) {
        super(userId, userType, username, password, firstName, surname, phoneNumber, email, photo, registered);
    }

    Savior(User user) {
        super(user.getUserId(), user.getUserType(), user.getUsername(), user.getPassword(), user.getFirstName(), user.getSurname(), user.getPhoneNumber(), user.getEmail(), user.getPhoto(), user.isRegistered());
    }

    public static ArrayList<Savior> saviorMapper(ResultSet rs) throws SQLException {
        ArrayList<Savior> listOfSaviors = new ArrayList<>();
        while (rs.next()) {
            //printResultSet(rs);
            Savior savior = new Savior(rs.getString("userId"), UserType.toEnum(rs.getString("userType")), rs.getString("username"), rs.getString("password"), rs.getString("firstName"), rs.getString("lastName"), rs.getString("phoneNumber"), rs.getString("email"), rs.getBytes("userPhoto"), rs.getBoolean("registered"));
            savior.stationId = rs.getString("stationId");
            savior.available = rs.getBoolean("available");
            savior.location = new Location(rs.getString("location"));
            listOfSaviors.add(savior);
        }
        return listOfSaviors;
    }

    private static void printResultSet(ResultSet rs) throws SQLException {
        ResultSetMetaData rsmd = rs.getMetaData();
        int columnsNumber = rsmd.getColumnCount();
        while (rs.next()) {
            for (int i = 1; i <= columnsNumber; i++) {
                if (i > 1) System.out.print(",  ");
                String columnValue = rs.getString(i);
                System.out.print(columnValue + " " + rsmd.getColumnName(i));
            }
            System.out.println();
        }
    }

    public static ArrayList<Action> getAllJoinableActions(String userId) {
        Savior savior = DatabaseUtil.getSaviorById(userId);
        assert savior != null;
        String SQL = "SELECT * FROM \"ActionRequests\";";
        ArrayList<ActionRequest> actionRequests = new ArrayList<>();
        try (Connection conn = DatabaseUtil.connectToDatabase();
             Statement stmt = conn.createStatement();
             ResultSet rs = stmt.executeQuery(SQL)) {
            actionRequests = ActionRequest.requestMapper(rs);
            DatabaseUtil.print("Successfully retrieved action requests");
        } catch (SQLException ex) {
            DatabaseUtil.err("Error when getting all action requests: " + ex.getMessage());
        }
        ArrayList<QualificationName> saviorQualifications = savior.getQualificationsEnum();
        ArrayList<Action> actions = new ArrayList<>();
        for (ActionRequest req : actionRequests) {
            if (req.getStationId().equals(savior.stationId)) {
                if (saviorQualifications.contains(req.getQualification())) {
                    actions.add(DatabaseUtil.getActionById(req.getActionId()));
                }
            }
        }
        return actions;
    }

    public static void makeAvailable(ArrayList<Savior> fetchSaviorIdsInAction) {
        for (Savior s : fetchSaviorIdsInAction) {
            s.updateAvailability(true);
        }
    }

    public static ArrayList<Savior> newSaviorMapper(ResultSet rs) throws SQLException {
        ArrayList<Savior> listOfSaviors = new ArrayList<>();
        while (rs.next()) {
            User user = User.getUserById(rs.getString("userId"));
            Savior savior = new Savior(user);
            savior.stationId = rs.getString("stationId");
            savior.location = new Location(rs.getString("location"));
            savior.available = rs.getBoolean("available");
            listOfSaviors.add(savior);
        }
        return listOfSaviors;
    }

    public static ArrayList<Savior> getUnassignedSaviors() {
        ArrayList<Savior> listOfUnassignedSaviors = new ArrayList<>();
        for (Savior s : DatabaseUtil.getAllSaviors()) {
            if (s.getStationId() == null) {
                listOfUnassignedSaviors.add(s);
            }
        }
        return listOfUnassignedSaviors;
    }

    public static void assignToStation(String stationId, String userId) {
        String SQL = "UPDATE \"Savior\" SET \"stationId\"= ? WHERE \"userId\" = ?";
        try (Connection conn = DatabaseUtil.connectToDatabase();
             PreparedStatement pstmt = conn.prepareStatement(SQL)) {
            pstmt.setString(1, stationId);
            pstmt.setString(2, userId);
            pstmt.executeUpdate();
        } catch (SQLException ex) {
            DatabaseUtil.print("Error while assigning savior to station: " + ex.getMessage());
            return;
        }
        DatabaseUtil.print("Savior assigned to station successfully");
    }

    public static void removeFromStation(String saviorId) {
        String SQL = "UPDATE \"Savior\" SET \"stationId\"= NULL WHERE \"userId\" = ?";

        try (Connection conn = DatabaseUtil.connectToDatabase();
             PreparedStatement pstmt = conn.prepareStatement(SQL)) {
            pstmt.setString(1, saviorId);
            pstmt.executeUpdate();
        } catch (SQLException ex) {
            DatabaseUtil.print("Error while unassigning savior from station: " + ex.getMessage());
            return;
        }
        DatabaseUtil.print("Savior unassigned from station successfully");
    }

    public static void makeLead(String userId) {
        String stationId = DatabaseUtil.getSaviorById(userId).getStationId();
        String locateSQL = "SELECT \"userId\" FROM \"User\" NATURAL JOIN \"Savior\" WHERE \"userType\"= 'LEAD_SAVIOR' AND \"stationId\" = '" + stationId + "';";
        String addSQL = "UPDATE \"User\" SET \"userType\"= 'LEAD_SAVIOR' WHERE \"userId\" = ?";
        try (Connection conn = DatabaseUtil.connectToDatabase();
             Statement stmt = conn.createStatement();
             PreparedStatement pstmt = conn.prepareStatement(addSQL)) {
            ResultSet rs = stmt.executeQuery(locateSQL);
            while (rs.next()) {
                makeSavior(rs.getString("userId"));
            }
            pstmt.setString(1, userId);
            pstmt.executeUpdate();
        } catch (SQLException ex) {
            DatabaseUtil.print("Error while making savior lead: " + ex.getMessage());
            return;
        }
        DatabaseUtil.print("Savior made lead successfully");
    }

    public static void makeSavior(String saviorId) {
        String SQL = "UPDATE \"User\" SET \"userType\"= 'SAVIOR' WHERE \"userId\" = ?";

        try (Connection conn = DatabaseUtil.connectToDatabase();
             PreparedStatement pstmt = conn.prepareStatement(SQL)) {
            pstmt.setString(1, saviorId);
            pstmt.executeUpdate();
        } catch (SQLException ex) {
            DatabaseUtil.print("Error while making savior lead: " + ex.getMessage());
            return;
        }
        DatabaseUtil.print("Savior made lead successfully");
    }

    public void updateLocation(Location loc) {
        location = loc;
        String SQL = "UPDATE \"Savior\" SET \"location\"= ? WHERE \"userId\" = ?";

        try (Connection conn = DatabaseUtil.connectToDatabase();
             PreparedStatement pstmt = conn.prepareStatement(SQL)) {
            pstmt.setString(1, location.toString());
            pstmt.setString(2, this.getUserId());
            pstmt.executeUpdate();
        } catch (SQLException ex) {
            DatabaseUtil.print("Error while updating savior location:" + ex.getMessage());
            return;
        }
        DatabaseUtil.print("Savior location updated successfully");
    }

    public String getStationName() {
        return DatabaseUtil.getStationById(stationId).getStationName();
    }

    public ActionInfo currentActionInfo() {
        String SQL = "SELECT * FROM \"SaviorsInAction\" WHERE \"userId\" = '" + this.getUserId() + "'";
        ActionInfo actionInfo = null;
        try (Connection conn = DatabaseUtil.connectToDatabase();
             Statement stmt = conn.createStatement();
             ResultSet rs = stmt.executeQuery(SQL)) {
            while (rs.next()) {
                actionInfo = new ActionInfo(rs.getString("actionId"), rs.getString("userId"), rs.getString("qualificationId"));
                DatabaseUtil.print("Successfully retrieved action info");
            }
        } catch (SQLException ex) {
            DatabaseUtil.err("Error while getting action info: " + ex.getMessage());
        }
        return actionInfo;
    }

    public void updateAvailability(boolean available) {
        this.available = available;
        String SQL = "UPDATE \"Savior\" SET \"available\"= ? WHERE \"userId\" = ?";

        try (Connection conn = DatabaseUtil.connectToDatabase();
             PreparedStatement pstmt = conn.prepareStatement(SQL)) {
            pstmt.setBoolean(1, available);
            pstmt.setString(2, this.getUserId());
            pstmt.executeUpdate();
        } catch (SQLException ex) {
            DatabaseUtil.print("Error while updating availability for savior:" + ex.getMessage());
            return;
        }
        DatabaseUtil.print("Availability updated successfully");
    }

    public void joinAction(String actionId, QualificationName qualificationName) {
        boolean updatedTable = false;
        String SQLQuery = "INSERT INTO \"SaviorsInAction\"" +
                "VALUES (?,?,?)";
        try (Connection conn = DatabaseUtil.connectToDatabase();
             PreparedStatement pstmt = conn.prepareStatement(SQLQuery,
                     Statement.RETURN_GENERATED_KEYS)) {
            pstmt.setString(1, actionId);
            pstmt.setString(2, this.getUserId());
            pstmt.setString(3, qualificationName.toString());
            int affectedRows = pstmt.executeUpdate();
            if (affectedRows > 0) {
                updatedTable = true;
            }
        } catch (SQLException ex) {
            DatabaseUtil.err("Error while joining action: " + ex.getMessage());
        }
        if (updatedTable) {
            updateAvailability(false);
            DatabaseUtil.print("Joined action successfully");
        } else
            DatabaseUtil.err("Error while joining action");
        ActionRequest.updateRequestNumber(actionId, stationId, qualificationName);
    }

    private Map<Task, Location> getSaviorTasks() {
        return null;
    }

    public String getStationId() {
        return stationId;
    }

    public void setStationId(String stationId) {
        this.stationId = stationId;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public ArrayList<QualificationName> getOtherQualifications() {
        ArrayList<QualificationName> qual = new ArrayList<>();
        ArrayList<QualificationName> currentQual = this.getQualificationsEnum();
        for (QualificationName q : EnumSet.allOf(QualificationName.class)) {
            if (!currentQual.contains(q)) {
                qual.add(q);
            }
        }
        return qual;
    }

    public List<SaviorQualification> getQualifications() {
        String SQL = "SELECT * FROM \"hasQualification\" NATURAL JOIN \"Qualification\" WHERE \"userId\" = '" + this.getUserId() + "'";
        ArrayList<SaviorQualification> listOfQualifications = new ArrayList<>();
        try (Connection conn = DatabaseUtil.connectToDatabase();
             Statement stmt = conn.createStatement();
             ResultSet rs = stmt.executeQuery(SQL)) {
            listOfQualifications = SaviorQualification.qualificationMapper(rs);
            DatabaseUtil.print("Successfully retrieved savior qualifications");
        } catch (SQLException ex) {
            DatabaseUtil.err("Error while getting savior qualificiations: " + ex.getMessage());
        }
        qualifications = listOfQualifications;
        return qualifications;
    }

    public void setQualifications(List<SaviorQualification> qualification) {
        this.qualifications = qualification;
    }

    public ArrayList<QualificationName> getQualificationsEnum() {
        ArrayList<QualificationName> qualifications = new ArrayList<>();
        for (SaviorQualification q : getQualifications())
            qualifications.add(q.getQualificationId());

        return qualifications;
    }

    public String saviorStationName(String stationId) {
        return DatabaseUtil.getStationById(stationId).getStationName();
    }

    public List<Task> getTasks() {
        String SQL = "SELECT \"taskId\",\"taskType\",\"comment\",\"Task\".\"location\", \"userId\" FROM \"Tasks\" NATURAL JOIN \"Task\" NATURAL JOIN \"Savior\";";
        ArrayList<Task> listOfTasks = new ArrayList<>();
        try (Connection conn = DatabaseUtil.connectToDatabase();
             Statement stmt = conn.createStatement();
             ResultSet rs = stmt.executeQuery(SQL)) {
            listOfTasks = Task.taskMapper(rs);
            DatabaseUtil.print("Successfully retrieved savior tasks");
        } catch (SQLException ex) {
            DatabaseUtil.err("Error when getting all saviors tasks: " + ex.getMessage());
        }
        tasks = listOfTasks;
        return tasks;
    }

    public void setTasks(List<Task> tasks) {
        this.tasks = tasks;
    }

    public QualificationName getCurrentActionQualification() {
        ActionInfo actionInfo = currentActionInfo();
        if (actionInfo != null) return actionInfo.getSaviorQualification();
        else return null;
    }

    public String getCurrentActionId() {
        ActionInfo actionInfo = currentActionInfo();
        if (actionInfo != null) return actionInfo.getActionId();
        else return null;
    }

    public Boolean getAvailable() {
        return available;
    }

    public void setAvailable(Boolean available) {
        this.available = available;
    }

    @Override
    public String toString() {
        return "Savior{" +
                "stationId='" + stationId + '\'' +
                ", location=" + location +
                ", qualifications=" + qualifications +
                ", tasks=" + tasks +
                ", available=" + available +
                '}';
    }
}
