package com.spring.teamice.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.EnumSet;

public class SaviorQualification {
    private QualificationName qualificationId;
    private String qualificationName;
    private int radiusOfCoverage;

    SaviorQualification(QualificationName qualificationId) {
        this.qualificationId = qualificationId;
        this.qualificationName = qualificationId.getName();
        this.radiusOfCoverage = qualificationId.coverage();
    }

    SaviorQualification(String qualificationId, String qualificationName, int radiusOfCoverage) {
        this.qualificationId = QualificationName.toEnum(qualificationId);
        this.qualificationName = qualificationName;
        this.radiusOfCoverage = radiusOfCoverage;
    }

    public static ArrayList<SaviorQualification> qualificationMapper(ResultSet rs) throws SQLException {
        ArrayList<SaviorQualification> listOfQualifications = new ArrayList<>();
        while (rs.next()) {
            SaviorQualification qualification = new SaviorQualification(rs.getString("qualificationId"), rs.getString("qualificationName"), rs.getInt("qualificationCoverage"));
            listOfQualifications.add(qualification);
        }
        return listOfQualifications;
    }

    public static void createQualifications() {
        ArrayList<SaviorQualification> qualifications = new ArrayList<>();
        for (QualificationName q : EnumSet.allOf(QualificationName.class)) {
            qualifications.add(new SaviorQualification(q));
        }
        String SQLQuery = "INSERT INTO \"Qualification\" VALUES (?,?,?)";
        try (Connection conn = DatabaseUtil.connectToDatabase();
             PreparedStatement pstmt = conn.prepareStatement(SQLQuery)) {
            for (SaviorQualification qual : qualifications) {
                pstmt.setString(1, qual.qualificationId.toString());
                pstmt.setString(2, qual.qualificationName);
                pstmt.setInt(3, qual.radiusOfCoverage);
                pstmt.addBatch();
            }
            pstmt.executeBatch();
        } catch (SQLException ex) {
            DatabaseUtil.err("Error while creating qualifications: " + ex.getMessage());
        }

        DatabaseUtil.print("Successfully created qualifications");
    }

    public QualificationName getQualificationId() {
        return qualificationId;
    }

    public void setQualificationId(QualificationName qualificationId) {
        this.qualificationId = qualificationId;
    }

    public String getQualificationName() {
        return qualificationName;
    }

    public void setQualificationName(String qualificationName) {
        this.qualificationName = qualificationName;
    }

    public int getRadiusOfCoverage() {
        return radiusOfCoverage;
    }

    public void setRadiusOfCoverage(int radiusOfCoverage) {
        this.radiusOfCoverage = radiusOfCoverage;
    }

    @Override
    public String toString() {
        return "SaviorQualification{" +
                "name=" + qualificationId +
                ", radiusOfCoverage=" + radiusOfCoverage +
                '}';
    }
}
