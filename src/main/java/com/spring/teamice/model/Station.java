package com.spring.teamice.model;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class Station {
    private String stationId;
    private String stationName;
    private StationType stationType;
    private Location stationLocation;
    private String stationLeadSavior;
    private ArrayList<Savior> stationSaviors;
    private Location location;
    private List<Savior> availableSaviors;

    public Station() {
    }

    public Station(String stationId, String stationName, StationType stationType, Location location) {
        this.stationId = stationId;
        this.stationName = stationName;
        this.stationType = stationType;
        this.location = location;
    }

    public Station(String stationName, StationType stationType, Location location) {
        this.stationId = UUID.randomUUID().toString();
        this.stationName = stationName;
        this.stationType = stationType;
        this.location = location;
    }

    public static ArrayList<Station> stationMapper(ResultSet rs) throws SQLException {
        ArrayList<Station> listOfStations = new ArrayList<>();
        while (rs.next()) {
            Station station = new Station(rs.getString("stationId"), rs.getString("stationName"), StationType.toEnum(rs.getString("stationType")), new Location(rs.getString("location")));
            listOfStations.add(station);
        }
        return listOfStations;
    }

    public static void deleteStation(String stationId) {
        String SQL = "DELETE FROM \"Station\" WHERE \"stationId\" = ?";

        try (Connection conn = DatabaseUtil.connectToDatabase();
             PreparedStatement pstmt = conn.prepareStatement(SQL)) {
            pstmt.setString(1, stationId);
            pstmt.executeUpdate();
        } catch (SQLException ex) {
            DatabaseUtil.print("Error while deleting station:" + ex.getMessage());
        }
        DatabaseUtil.print("Station deleted successfully");
    }

    public void getCompleteStation() {
        Station previousStation = getStationById(this.stationId);
        if (previousStation != null) {
            this.stationName = (this.stationName == null) ? previousStation.stationName : this.stationName;
            this.stationType = (this.stationType == null) ? previousStation.stationType : this.stationType;
            this.location = (this.location == null) ? previousStation.location : this.location;
        } else {
            DatabaseUtil.err("Station not found while gettting complete station");
        }
    }

    private Station getStationById(String stationId) {
        String SQL = "SELECT * FROM \"Station\" WHERE \"stationId\" = '" + stationId + "'";
        Station result;
        try (Connection conn = DatabaseUtil.connectToDatabase();
             Statement stmt = conn.createStatement();
             ResultSet rs = stmt.executeQuery(SQL)) {
            result = stationMapper(rs).get(0);
        } catch (SQLException ex) {
            DatabaseUtil.err("Error while getting station by user id: " + ex.getMessage());
            return null;
        }
        return result;
    }

    public void storeStation() {
        String SQLQuery = "" +
                "INSERT INTO \"Station\"" +
                "VALUES (?,?,?,?)";
        try (Connection conn = DatabaseUtil.connectToDatabase();
             PreparedStatement pstmt = conn.prepareStatement(SQLQuery,
                     Statement.RETURN_GENERATED_KEYS)) {
            pstmt.setString(1, this.stationId);
            pstmt.setString(2, this.stationName);
            pstmt.setString(3, this.stationType.toString());
            pstmt.setString(4, this.location.toString());

            int affectedRows = pstmt.executeUpdate();
            if (affectedRows > 0) {
                DatabaseUtil.print("Station stored successfully: " + this);
            }
        } catch (SQLException ex) {
            DatabaseUtil.err("Error while storing station: " + ex.getMessage());
        }
    }

    public void deleteStation() {
        String SQL = "DELETE FROM \"Station\" WHERE \"stationId\" = ?";

        try (Connection conn = DatabaseUtil.connectToDatabase();
             PreparedStatement pstmt = conn.prepareStatement(SQL)) {

            pstmt.setString(1, stationId);
            pstmt.executeUpdate();
        } catch (SQLException ex) {
            DatabaseUtil.print("Error while deleting station:" + ex.getMessage());
        }
        DatabaseUtil.print("Station deleted successfully");
    }

    public String countAvailableSaviors() {
        int a = 0;
        int b = 0;

        for (Savior savior : this.fetchStationSaviors()) {
            a++;
            if (savior.getAvailable()) b++;
        }

        String ratio = b + "/" + a;
        return ratio;
    }

    public boolean updateStation() {
        DatabaseUtil.print("Station update: " + this.toString());
        String SQL = "UPDATE \"Station\" " +
                "SET " +
                "\"stationId\" = ?" +
                " , \"stationName\" = ?" +
                " , \"stationType\" = ?" +
                " , \"location\" = ?" +
                " WHERE \"stationId\" = ?";

        try (Connection conn = DatabaseUtil.connectToDatabase();
             PreparedStatement pstmt = conn.prepareStatement(SQL)) {
            pstmt.setString(1, this.stationId);
            pstmt.setString(2, this.stationName);
            pstmt.setString(3, this.stationType.toString());
            pstmt.setString(4, this.location.toString());
            pstmt.setString(5, this.stationId);
            pstmt.executeUpdate();
        } catch (SQLException ex) {
            DatabaseUtil.print("Error while updating station: " + ex.getMessage());
            return false;
        }
        DatabaseUtil.print("Station deleted successfully");
        return true;
    }

    public String getStationId() {
        return stationId;
    }

    public void setStationId(String stationId) {
        this.stationId = stationId;
    }

    public String getStationName() {
        return stationName;
    }

    public void setStationName(String stationName) {
        this.stationName = stationName;
    }

    public StationType getStationType() {
        return stationType;
    }

    public void setStationType(StationType stationType) {
        this.stationType = stationType;
    }

    public Location getStationLocation() {
        return stationLocation;
    }

    public void setStationLocation(Location stationLocation) {
        this.stationLocation = stationLocation;
    }

    public String getStationLeadSavior() {
        return stationLeadSavior;
    }

    public void setStationLeadSavior(String stationLeadSavior) {
        this.stationLeadSavior = stationLeadSavior;
    }

    public ArrayList<Savior> fetchStationSaviors() {
        ArrayList<Savior> stationSaviors = new ArrayList<>();
        for (Savior savior : DatabaseUtil.getAllSaviors()) {
            if (savior.getStationId() != null)
                if (savior.getStationId().equals(stationId))
                    stationSaviors.add(savior);
        }
        this.stationSaviors = stationSaviors;
        return this.stationSaviors;
    }

    public void setStationSaviors(ArrayList<Savior> stationSaviors) {
        this.stationSaviors = stationSaviors;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public List<Savior> fetchAllAvailableSaviors() {
        ArrayList<Savior> availableSaviors = new ArrayList<>();
        for (Savior savior : DatabaseUtil.getAllSaviors()) {
            if (savior.getAvailable()) {
                availableSaviors.add(savior);
            }
        }
        this.availableSaviors = availableSaviors;
        return this.availableSaviors;
    }

    public void setAvailableSaviors(List<Savior> availableSaviors) {
        this.availableSaviors = availableSaviors;
    }

    @Override
    public String toString() {
        return "Station{" +
                "stationName='" + stationName + '\'' +
                ", stationType=" + stationType +
                ", stationLocation=" + stationLocation +
                ", stationLeadSavior=" + stationLeadSavior +
                ", stationSaviors=" + stationSaviors +
                ", availableSaviors=" + availableSaviors +
                '}';
    }

}
