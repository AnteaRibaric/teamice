package com.spring.teamice.model;

public enum StationType {
    PERMANENT_STATION,
    TEMPORARY_STATION;

    public static StationType toEnum(String type) {
        if (StationType.PERMANENT_STATION.toString().equals(type)) return StationType.PERMANENT_STATION;
        else if (StationType.TEMPORARY_STATION.toString().equals(type)) return StationType.TEMPORARY_STATION;
        else return null;
    }

    public String toCroWord() {
        switch (this) {
            case PERMANENT_STATION -> {
                return "Trajna postaja";
            }
            case TEMPORARY_STATION -> {
                return "Privremena postaja";
            }
            default -> {
                return null;
            }
        }
    }
}
