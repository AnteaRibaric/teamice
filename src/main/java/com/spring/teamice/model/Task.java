package com.spring.teamice.model;

import java.sql.*;
import java.util.ArrayList;
import java.util.UUID;

public class Task {
    private String taskId;
    private TaskType taskType;
    private Location location;
    private String comment;

    public Task(String taskId, TaskType taskType, Location location, String comment) {
        this.taskId = taskId;
        this.taskType = taskType;
        this.location = location;
        this.comment = comment;
    }

    public Task(TaskType taskType, Location location, String comment) {
        taskId = UUID.randomUUID().toString();
        this.taskType = taskType;
        this.location = location;
        this.comment = comment;
    }

    public static ArrayList<Task> taskMapper(ResultSet rs) throws SQLException {
        ArrayList<Task> listOfTasks = new ArrayList<>();
        while (rs.next()) {
            Task task = new Task(rs.getString("taskId"), TaskType.toEnum(rs.getString("taskType")), new Location(rs.getString("location")), rs.getString("comment"));
            listOfTasks.add(task);
        }
        return listOfTasks;
    }

    public static void deleteActionTasks(String actionId) {
        String SQL = "SELECT \"taskId\" FROM \"Tasks\" WHERE \"actionId\" = '" + actionId + "';";
        ArrayList<String> listOfTaskIds = new ArrayList<>();
        try (Connection conn = DatabaseUtil.connectToDatabase();
             Statement stmt = conn.createStatement();
             ResultSet rs = stmt.executeQuery(SQL)) {
            while (rs.next()) {
                listOfTaskIds.add(rs.getString("taskId"));
            }
            if (!listOfTaskIds.isEmpty()) {
                String deleteSQL = "DELETE FROM \"Task\" WHERE \"userId\" IN (";
                for (String task : listOfTaskIds) {
                    if (listOfTaskIds.get(0).equals(task))
                        deleteSQL += task;
                    else
                        deleteSQL += "," + task;
                }
                deleteSQL += ");";
                PreparedStatement pstmt = conn.prepareStatement(deleteSQL);
                pstmt.executeUpdate();
            }
        } catch (SQLException ex) {
            DatabaseUtil.err("Error while deleting tasks from action: " + ex.getMessage());
        }
        DatabaseUtil.print("Successfully deleted tasks from action");
    }

    public void deleteTask() {
        String SQL = "DELETE FROM \"Task\" WHERE \"taskId\" = ?";

        try (Connection conn = DatabaseUtil.connectToDatabase();
             PreparedStatement pstmt = conn.prepareStatement(SQL)) {

            pstmt.setString(1, taskId);
            pstmt.executeUpdate();
        } catch (SQLException ex) {
            DatabaseUtil.print("Error while deleting task:" + ex.getMessage());
        }
        DatabaseUtil.print("Task deleted successfully");
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public TaskType getTaskType() {
        return taskType;
    }

    public void setTaskType(TaskType taskType) {
        this.taskType = taskType;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
    public String toString() {
        return "Task{" +
                "taskId='" + taskId + '\'' +
                ", taskType=" + taskType +
                ", location=" + location +
                ", comment='" + comment + '\'' +
                '}';
    }
}
