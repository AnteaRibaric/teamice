package com.spring.teamice.model;

public enum TaskType {
    SEARCH_ROUTE,
    GO_TO_LOCATION,
    SETUP_TEMPORARY_STATION,
    REFRESH_TEMPORARY_STATION_STOCK;

    public static TaskType toEnum(String type) {
        if (TaskType.SEARCH_ROUTE.toString().equals(type)) return TaskType.SEARCH_ROUTE;
        else if (TaskType.GO_TO_LOCATION.toString().equals(type)) return TaskType.GO_TO_LOCATION;
        else if (TaskType.SETUP_TEMPORARY_STATION.toString().equals(type)) return TaskType.SETUP_TEMPORARY_STATION;
        else if (TaskType.REFRESH_TEMPORARY_STATION_STOCK.toString().equals(type))
            return TaskType.REFRESH_TEMPORARY_STATION_STOCK;
        else return null;
    }

    public String toCroWord() {
        switch (this) {
            case SEARCH_ROUTE -> {
                return "Istraži rutu";
            }
            case GO_TO_LOCATION -> {
                return "Odi na lokaciju";
            }
            case SETUP_TEMPORARY_STATION -> {
                return "Postavi privremenu postaju";
            }
            case REFRESH_TEMPORARY_STATION_STOCK -> {
                return "Obnovi zalihu privremene postaje";
            }
            default -> {
                return null;
            }
        }
    }
}

