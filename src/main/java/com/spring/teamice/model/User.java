package com.spring.teamice.model;

import java.sql.*;
import java.util.ArrayList;
import java.util.UUID;

public class User {
    private String userId;
    private UserType userType;
    private String username;
    private String password;
    private String firstName;
    private String surname;
    private String phoneNumber;
    private String email;
    private Location location;
    private byte[] photo;
    private boolean registered = false;

    public User() {
    }

    public User(String userId, UserType userType, String username, String password, String firstName, String surname, String phoneNumber, String email, byte[] photo, boolean registered) {
        this.userId = userId;
        this.userType = userType;
        this.username = username;
        this.password = password;
        this.firstName = firstName;
        this.surname = surname;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.registered = registered;
        this.photo = photo;
    }

    public static boolean storeUser(User user) {
        String SQLQuery = "" +
                "INSERT INTO \"User\"" +
                "VALUES (?,?,?,?,?,?,?,?,?,?)";
        try (Connection conn = DatabaseUtil.connectToDatabase();
             PreparedStatement pstmt = conn.prepareStatement(SQLQuery,
                     Statement.RETURN_GENERATED_KEYS)) {
            pstmt.setString(1, user.userId);
            pstmt.setString(2, user.username);
            pstmt.setString(3, user.password);
            pstmt.setString(4, user.firstName);
            pstmt.setString(5, user.userType.toString());
            pstmt.setString(6, user.surname);
            pstmt.setString(7, user.phoneNumber);
            pstmt.setString(8, user.email);
            pstmt.setBoolean(9, user.registered);
            pstmt.setBytes(10, user.photo);

            int affectedRows = pstmt.executeUpdate();
            if (affectedRows > 0) {
                DatabaseUtil.print("User stored successfully: " + user);
            }
        } catch (SQLException ex) {
            DatabaseUtil.err("Error while storing user: " + ex.getMessage());
            return false;
        }
        return true;
    }

    public static boolean usernameTaken(String usrname) {
        String SQL = "SELECT * FROM \"User\" WHERE username = '" + usrname + "'";
        try (Connection conn = DatabaseUtil.connectToDatabase();
             Statement stmt = conn.createStatement();
             ResultSet rs = stmt.executeQuery(SQL)) {
            if (rs.next()) {
                DatabaseUtil.err("Username already exists");
                return true;
            }
        } catch (SQLException ex) {
            DatabaseUtil.err("Error while checking if username exists: " + ex.getMessage());
            return true;
        }
        return false;
    }

    public static LoginStatus successfulLogin(String username, String password) {
        LoginStatus returnStatus = new LoginStatus();
        String SQLQuery = "SELECT \"userId\", username, password, registered FROM \"User\"";
        try (Connection conn = DatabaseUtil.connectToDatabase();
             Statement stmt = conn.createStatement();
             ResultSet rs = stmt.executeQuery(SQLQuery)) {

            while (rs.next()) {
                if (rs.getString("username").equals(username.trim())) {
                    if (rs.getString("password").equals(password)) {
                        if (rs.getBoolean("registered")) {
                            returnStatus.setSuccessfullLogin(true);
                            returnStatus.setUserId(rs.getString("userId"));
                            return returnStatus;
                        } else {
                            returnStatus.setSuccessfullLogin(false);
                            returnStatus.setMessage("Admin još nije prihvatio Vaš zahtjev");
                            DatabaseUtil.err("User is not accepted by admin yet");
                            return returnStatus;
                        }
                    } else {
                        DatabaseUtil.err("Invalid password");
                        returnStatus.setSuccessfullLogin(false);
                        returnStatus.setMessage("Netočna šifra ili korisničko ime");
                        return returnStatus;
                    }
                }
            }
            returnStatus.setSuccessfullLogin(false);
            returnStatus.setMessage("Netočna šifra ili korisničko ime");
            DatabaseUtil.err("User does not exist in our database");
            return returnStatus;
        } catch (SQLException ex) {
            DatabaseUtil.err("Error while validating login:" + ex.getMessage());
            returnStatus.setSuccessfullLogin(false);
            return returnStatus;
        }
    }

    public static ArrayList<User> userMapper(ResultSet rs) throws SQLException {
        ArrayList<User> listOfUsers = new ArrayList<>();
        while (rs.next()) {
            User user = new User();
            user.userId = rs.getString("userId");
            user.username = rs.getString("username");
            user.password = rs.getString("password");
            user.firstName = rs.getString("firstName");
            user.surname = rs.getString("lastName");
            user.phoneNumber = rs.getString("phoneNumber");
            user.email = rs.getString("email");
            user.registered = rs.getBoolean("registered");
            user.userType = UserType.toEnum(rs.getString("userType"));
            user.photo = rs.getBytes("userPhoto");
            listOfUsers.add(user);
        }
        return listOfUsers;
    }

    public static UserType getUserTypeEnum(String userId) {
        return getUserById(userId).userType;
    }

    public static void deleteUser(String userId) {
        String SQL = "DELETE FROM \"User\" WHERE \"userId\" = ?";

        try (Connection conn = DatabaseUtil.connectToDatabase();
             PreparedStatement pstmt = conn.prepareStatement(SQL)) {

            pstmt.setString(1, userId);
            pstmt.executeUpdate();
        } catch (SQLException ex) {
            DatabaseUtil.print("Error while deleting user:" + ex.getMessage());
        }
        DatabaseUtil.print("User deleted successfully");
    }

    public static User getUserById(String userId) {
        for (User user : DatabaseUtil.getAllUsers()) {
            if (user.userId.equals(userId)) {
                return user;
            }
        }
        DatabaseUtil.print("User doesn't exist in our database");
        return null;
    }

    public static boolean acceptUser(String userId) {
        String SQL = "UPDATE \"User\" SET \"registered\"= true WHERE \"userId\" = ?";

        try (Connection conn = DatabaseUtil.connectToDatabase();
             PreparedStatement pstmt = conn.prepareStatement(SQL)) {

            pstmt.setString(1, userId);
            pstmt.executeUpdate();
        } catch (SQLException ex) {
            DatabaseUtil.print("Error while accepting user:" + ex.getMessage());
            return false;
        }
        if (getUserById(userId).isSavior()) {
            Savior savior = DatabaseUtil.getSaviorById(userId);
            if (savior != null)
                savior.updateAvailability(true);
        }
        DatabaseUtil.print("User accepted successfully");
        return true;
    }

    public static ArrayList<String> getAllAdminIds() {
        String SQL = "SELECT * FROM \"Admin\";";
        ArrayList<String> listOfAdminIds = new ArrayList<>();
        try (Connection conn = DatabaseUtil.connectToDatabase();
             Statement stmt = conn.createStatement();
             ResultSet rs = stmt.executeQuery(SQL)) {

            while (rs.next()) {
                String adminId = rs.getString("userId");
                listOfAdminIds.add(adminId);
            }

        } catch (SQLException ex) {
            DatabaseUtil.err("Error while getting admin ids: " + ex.getMessage());
        }
        DatabaseUtil.print("Got admins user ids successfully");
        return listOfAdminIds;
    }

    public static boolean makeAdmin(String userId) {
        User user = getUserById(userId);
        if (user != null) {
            user.userType = UserType.ADMIN;
            user.updateUser();
        } else return false;
        return true;
    }

    public boolean registerUser() {
        this.username = this.username.trim();
        this.userId = UUID.randomUUID().toString();
        String SQL = "INSERT INTO ";
        if (this.userType.equals(UserType.DISPATCHER)) {
            SQL += "\"Dispatcher\" ";
        } else if (this.userType.equals(UserType.SAVIOR) || this.userType.equals(UserType.LEAD_SAVIOR)) {
            SQL += "\"Savior\" ";
        } else if (this.userType.equals(UserType.ADMIN)) {
            SQL += "\"Admin\" ";
        } else {
            DatabaseUtil.err(this.userType + " is invalid role");
            return false;
        }
        SQL += "VALUES(?)";
        if (storeUser(this)) {
            try (Connection conn = DatabaseUtil.connectToDatabase();
                 PreparedStatement pstmt = conn.prepareStatement(SQL)) {
                pstmt.setString(1, this.userId);
                pstmt.executeUpdate();
                DatabaseUtil.print("User type inserted for user:" + this);
            } catch (SQLException ex) {
                DatabaseUtil.err("Error while inserting userType:" + ex.getMessage());
                return false;
            }
        } else return false;
        return true;
    }

    public boolean isDispatcher() {
        return userType.equals(UserType.DISPATCHER);
    }

    public boolean isSavior() {
        return userType.equals(UserType.LEAD_SAVIOR) || userType.equals(UserType.SAVIOR);
    }

    public boolean isAdmin() {
        return userType.equals(UserType.ADMIN);
    }

    public boolean isLeadSavior() {
        return userType.equals(UserType.LEAD_SAVIOR);
    }


    public boolean updateUser() {
        if (this.userId == null) {
            DatabaseUtil.print("User id is null, unable to update user");
            return false;
        }
        User completeUser = this.getCompleteUser();
        DatabaseUtil.print("User update: " + completeUser);
        String SQL = "UPDATE \"User\" " +
                "SET " +
                " \"userId\" = ?" +
                " , \"username\" = ?" +
                " , \"password\" = ?" +
                ",  \"userType\" = ?" +
                " , \"firstName\" = ?" +
                " , \"lastName\"= ?" +
                " , \"phoneNumber\"= ?" +
                " , \"email\"= ?" +
                " , \"registered\"= ?" +
                " , \"userPhoto\"= ?" +
                "WHERE \"userId\" = ?";
        try (Connection conn = DatabaseUtil.connectToDatabase();
             PreparedStatement pstmt = conn.prepareStatement(SQL)) {
            pstmt.setString(1, completeUser.userId);
            pstmt.setString(2, completeUser.username);
            pstmt.setString(3, completeUser.password);
            pstmt.setString(4, completeUser.userType.toString());
            pstmt.setString(5, completeUser.firstName);
            pstmt.setString(6, completeUser.surname);
            pstmt.setString(7, completeUser.phoneNumber);
            pstmt.setString(8, completeUser.email);
            pstmt.setBoolean(9, completeUser.registered);
            pstmt.setBytes(10, completeUser.photo);
            pstmt.setString(11, completeUser.userId);
            pstmt.executeUpdate();
        } catch (SQLException ex) {
            DatabaseUtil.err("Error while updating user: " + ex.getMessage());
            return false;
        }
        if (completeUser.updateUserType()) {
            DatabaseUtil.print("User updated successfully");
            return true;
        } else return false;
    }

    private boolean updateUserType() {
        String SQL;
        UserType role = getUserTypeEnum(this.userId);
        if (role != this.userType) {
            switch (this.userType) {
                case DISPATCHER -> SQL = "INSERT INTO \"Dispatcher\" VALUES(?)";
                case LEAD_SAVIOR, SAVIOR -> SQL = "INSERT INTO \"Savior\" VALUES(?)";
                case ADMIN -> SQL = "INSERT INTO \"Admin\" VALUES(?)";
                default -> {
                    return false;
                }
            }

            try (Connection conn = DatabaseUtil.connectToDatabase();
                 PreparedStatement pstmt = conn.prepareStatement(SQL)) {
                pstmt.setString(1, userId);
                pstmt.executeUpdate();
            } catch (SQLException ex) {
                DatabaseUtil.err("Error while inserting role: " + ex.getMessage());
                return false;
            }
            DatabaseUtil.print("Inserted new role");
            if (role != null) {
                switch (role) {
                    case DISPATCHER -> SQL = "DELETE FROM \"Dispatcher\" WHERE \"userId\" = ?";
                    case LEAD_SAVIOR, SAVIOR -> SQL = "DELETE FROM \"Savior\" WHERE \"userId\" = ?";
                    case ADMIN -> SQL = "DELETE FROM \"Admin\" WHERE \"userId\" = ?";
                    default -> {
                        return false;
                    }
                }
                try (Connection conn = DatabaseUtil.connectToDatabase();
                     PreparedStatement pstmt = conn.prepareStatement(SQL)) {
                    pstmt.setString(1, userId);
                    pstmt.executeUpdate();
                } catch (SQLException ex) {
                    DatabaseUtil.err("Error while deleting old role:" + ex.getMessage());
                    return false;
                }
            }
        } else return false;
        return true;
    }

    public User getCompleteUser() {
        User previousUser = getUserById(this.userId);
        if (previousUser != null) {
            this.firstName = (this.firstName == null) ? previousUser.firstName : this.firstName;
            this.userType = (this.userType == null) ? previousUser.userType : this.userType;
            this.surname = (this.surname == null) ? previousUser.surname : this.surname;
            this.username = (this.username == null) ? previousUser.username : this.username;
            this.password = (this.password == null) ? previousUser.password : this.password;
            this.phoneNumber = (this.phoneNumber == null) ? previousUser.phoneNumber : this.phoneNumber;
            this.email = (this.email == null) ? previousUser.email : this.email;
            this.registered = previousUser.registered || this.registered;
            this.photo = (this.photo == null) ? previousUser.photo : this.photo;
        }
        DatabaseUtil.print("User type is: " + this.userType);
        return this;
    }

    public boolean signUpIncomplete() {
        return this.firstName.length() < 1 || this.surname.length() < 1 || this.userType == null || this.username.length() < 5
                || this.phoneNumber.length() < 9 || this.password.length() < 8 || this.email.length() < 5;
    }


    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public UserType getUserType() {
        return userType;
    }

    public void setUserType(UserType userType) {
        this.userType = userType;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    public boolean isRegistered() {
        return registered;
    }

    public void setRegistered(boolean registered) {
        this.registered = registered;
    }

    @Override
    public String toString() {
        return "User{" +
                "userId='" + userId + '\'' +
                ", userType=" + userType +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", firstName='" + firstName + '\'' +
                ", surname='" + surname + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", email='" + email + '\'' +
                ", registered=" + registered +
                '}';
    }
}
