package com.spring.teamice.model;

public enum UserType {
    DISPATCHER,
    SAVIOR,
    LEAD_SAVIOR,
    ADMIN;

    public static UserType toEnum(String type) {
        if (UserType.ADMIN.toString().equals(type)) return UserType.ADMIN;
        else if (UserType.SAVIOR.toString().equals(type)) return UserType.SAVIOR;
        else if (UserType.LEAD_SAVIOR.toString().equals(type)) return UserType.LEAD_SAVIOR;
        else if (UserType.DISPATCHER.toString().equals(type)) return UserType.DISPATCHER;
        else return null;
    }

    public String toCroWord(UserType role) {
        switch (role) {
            case SAVIOR -> {
                return "Spasilac";
            }
            case ADMIN -> {
                return "Admin";
            }
            case DISPATCHER -> {
                return "Dispečer";
            }
            case LEAD_SAVIOR -> {
                return "Glavni spasilac";
            }
            default -> {
                return null;
            }
        }
    }
}
