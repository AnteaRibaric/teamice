package com.spring.teamice.model;

import java.sql.*;
import java.util.ArrayList;

public class WayPoint {
    private String userId;
    private String wayPointId;
    private Location location;

    public WayPoint(String userId, String wayPointId, Location location) {
        this.userId = userId;
        this.wayPointId = wayPointId;
        this.location = location;
    }

    public static ArrayList<WayPoint> wayPointMapper(ResultSet rs) throws SQLException {
        ArrayList<WayPoint> listOfWayPoints = new ArrayList<>();
        while (rs.next()) {
            WayPoint wayPoint = new WayPoint(rs.getString("userId"), rs.getString("wayPointId"), new Location((rs.getString("location"))));
            listOfWayPoints.add(wayPoint);
        }
        return listOfWayPoints;
    }

    public void storeWayPoint() {
        String SQLQuery = "" +
                "INSERT INTO \"WayPoint\"" +
                "VALUES (?,?,?)";
        try (Connection conn = DatabaseUtil.connectToDatabase();
             PreparedStatement pstmt = conn.prepareStatement(SQLQuery,
                     Statement.RETURN_GENERATED_KEYS)) {
            pstmt.setString(1, this.userId);
            pstmt.setString(2, this.wayPointId);
            pstmt.setString(3, this.location.toString());

            int affectedRows = pstmt.executeUpdate();
            if (affectedRows > 0) {
                DatabaseUtil.print("Waypoint stored successfully: " + this);
            }
        } catch (SQLException ex) {
            DatabaseUtil.err("Error while storing waypoint: " + ex.getMessage());
        }
    }

    public void deleteWaypoint() {
        String SQL = "DELETE FROM \"WayPoint\" WHERE \"wayPointId\" = ?";

        try (Connection conn = DatabaseUtil.connectToDatabase();
             PreparedStatement pstmt = conn.prepareStatement(SQL)) {

            pstmt.setString(1, wayPointId);
            pstmt.executeUpdate();
        } catch (SQLException ex) {
            DatabaseUtil.print("Error while deleting waypoint:" + ex.getMessage());
        }
        DatabaseUtil.print("Waypoint deleted successfully");
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getWayPointId() {
        return wayPointId;
    }

    public void setWayPointId(String wayPointId) {
        this.wayPointId = wayPointId;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    @Override
    public String toString() {
        return "WayPoint{" +
                "wayPointId='" + wayPointId + '\'' +
                ", location=" + location +
                '}';
    }


}
