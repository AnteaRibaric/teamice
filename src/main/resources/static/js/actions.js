var coll = document.getElementsByClassName("collapsible");
var i;

for (i = 0; i < coll.length; i++) {
    coll[i].addEventListener("click", function () {
        var content = this.parentElement.parentElement.nextElementSibling;
        while (content.classList.contains("content")) {
            if (content.style.display === "table-row") {
                content.style.display = "none";
            } else {
                content.style.display = "table-row";
            }
            content = content.nextElementSibling;
        }
    });
}

var overlay;

function openRequestForm(actionId) {
    let form = document.getElementById("request_form");
    form.actionId.value = actionId;
    overlay = document.getElementById("overlay");
    overlay.style.display = "block";
}

function closeRequestForm() {
    overlay.style.display = "none";
}
