function validatePwd() {
    let pwd1 = document.getElementById("password1");
    let pwd2 = document.getElementById("password2");
    if (pwd1.value.length < 8 || pwd1.value.length > 50) {
        pwd1.style.border = '2px solid red';
        return false;
    } else if (!(pwd1.value.length < 8 || pwd1.value.length > 50)) {
        pwd1.style.border = '2px solid green';

    }

    if (pwd2.value.length < 8 || pwd2.value.length > 50) {
        pwd2.style.border = '2px solid red';
        return false;
    } else if (!(pwd2.value.length < 8 || pwd2.value.length > 50)) {
        pwd2.style.border = '2px solid green';

    }

    if (pwd1.value && pwd2.value && pwd1.value !== pwd2.value) {
        pwd1.style.border = '2px solid red';
        pwd2.style.border = '2px solid red';
        return false;
    } else {
        pwd1.style.border = '2px solid green';
        pwd2.style.border = '2px solid green';
        return true;
    }

}

let pwd1 = document.getElementById("password1");
let pwd2 = document.getElementById("password2");

pwd1.onfocusout = function () {
    validatePwd()
};


pwd2.onfocusout = function () {
    validatePwd()
};

const avatar = document.querySelector('input[name="photoUser"]');
const preview = document.querySelector('.preview');

avatar.addEventListener('change', updateImageDisplay);

function updateImageDisplay() {
    while (preview.firstChild) {
        preview.removeChild(preview.firstChild);
    }

    const curFiles = avatar.files;
    if (curFiles.length === 0) {
        const para = document.createElement('p');
        para.textContent = 'No files currently selected for upload';
        preview.appendChild(para);
    } else {
        const list = document.createElement('ul');
        list.style.listStyleType = "none"
        preview.appendChild(list);

        for (const file of curFiles) {
            const listItem = document.createElement('li');
            const para = document.createElement('p');
            if (validFileType(file)) {
                const image = document.createElement('img');
                image.src = URL.createObjectURL(file);
                image.className = "photo";
                listItem.appendChild(image);
                listItem.appendChild(para);
            } else {
                para.textContent = `File name ${file.name}: Not a valid file type. Update your selection.`;
                listItem.appendChild(para);
            }

            list.appendChild(listItem);
        }
    }
}

const fileTypes = [
    "image/apng",
    "image/bmp",
    "image/gif",
    "image/jpeg",
    "image/pjpeg",
    "image/png",
    "image/svg+xml",
    "image/tiff",
    "image/webp",
    "image/x-icon"
];

function validFileType(file) {
    return fileTypes.includes(file.type);
}
