var coll = document.getElementsByClassName("collapsible");
var i;

for (i = 0; i < coll.length; i++) {
    coll[i].addEventListener("click", function () {
        var content = this.parentElement.parentElement.nextElementSibling;

        while (content.classList.contains("content")) {
            if (content.style.display === "table-row") {
                content.style.display = "none";
            } else {
                content.style.display = "table-row";
            }
            content = content.nextElementSibling;
        }
    });
}

